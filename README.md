# Ulm_Weatherballoon

![Weatherballoon in about 30km height](https://gitlab.com/F-Schmidt/ulm_weatherballoon/-/raw/main/assets/Weatherballoon_30km.png)

See this fascinating image from our earth?
At Ulm University we started building weather ballons in 2021 and since then had several different flights to a height
of up to 40 km (for the non-metrics: about 25 miles), which is about three to four times the height of a normal flight.

Sending electronics in such a height also requires well-tested and established software, so this repository mostly
contains practically tested software in rough environments. A lot of well-known software-libraries are used with
an additional lightweight software-layer to hide some nasty details and configure sensors correctly. Please note,
that this software is implemented to work with the Arduino Framework.

All further information are listed in the wiki of this repository.

Falko Schmidt, 2023
