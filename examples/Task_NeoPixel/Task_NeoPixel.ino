#include <Arduino.h>                // <- Import Arduino-functions
#include <Ulm_Weatherballoon.h>     // <- Import weather-balloon components

// This can be used to control the LED.
// Please note, that you must adapt the pin, which the
// LED is connected to in your circuit.
constexpr uint8_t MY_LED_PIN = /*TODO*/;
Ulm_NeoPixel led = Ulm_NeoPixel(MY_LED_PIN);

void setup() {
    // TODO Your task here:
    //  Initialize the RGB LED.
}


void loop() {
    // TODO Your task here:
    //  Turn LED on in red color.
    //  Wait 500 milliseconds.
    //  Turn LED on in yellow color.
    //  Wait 500 milliseconds.
    //  Turn LED on in green color.
    //  Wait 500 milliseconds.
    //  Repeat.
}
