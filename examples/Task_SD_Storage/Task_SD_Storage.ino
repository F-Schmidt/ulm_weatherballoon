#include <Arduino.h>
#include "Ulm_Weatherballoon.h"

Ulm_SDStorage storage = Ulm_SDStorage::builder()
        .atPin(/* TODO */)
        .withDetectPin(/* TODO */)
        .atDirectory(/* TODO */)
        .withDataFile(/* TODO */)
        .withRedundancyFile(/* TODO */)
        .build();

void setup() {
    Serial.begin(115200);
    while(!Serial) {}
    Serial.println("Running SD-Storage task...");

    // TODO: Please init the SD storage.
    //  You must check the return value. If it is false,
    //  then there is an error in the configuration above,
    //  that you must fix! Please output useful messages to Serial.
}

/**
 * This function runs forever.
 * We will use it to implement our sampling and storage routine.
 */
void loop() {
    // TODO: Write a loop, that stores these messages to the SD storage:
    //  "Saving message 1"
    //  "Saving message 2"
    //  ...
    //  After implementation, let the program run for a bit (2-3 minutes) and
    //  check, if all messages are properly stored onto the SD card. Please check
    //  both the data and the redundancy file.
    //  Please also make sure, to add a 1 second delay after each writing.
    //  You may optionally also output the messages to Serial.
}
