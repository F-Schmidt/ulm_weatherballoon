/*
 * Minimum working example to control an SCD40/SCD41 sensor using the
 * Ulm_Weatherballoon library.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_Weatherballoon.h"

// The CO2 sensor
auto sensor = Ulm_SCD4X();

// A data sample from the CO2 sensor
Ulm_SCD4X_Sample sensorSample = {};

/**
 * This function will run once at the beginning
 * and will initialise all interfaces and sensors.
 */
void setup() {
    // Start serial connection for message outputs.
    Serial.begin(115200);
    while(!Serial) {}

    // Start CO2 sensor and check if it works.
    if(sensor.begin() == true) {
        // CO2 sensor works
        Serial.println("SCD4X sensor initialized successfully.");
        Serial.print("Waiting for the first sensor sample. This will take 5 seconds");
        for(uint8_t i = 0; i < 10; i++) {
            Serial.print('.');
            delay(500);
        }
        Serial.println("done.");
        Serial.println("Sensor is ready.");
    } else {
        // CO2 sensor or interface does NOT work!
        Serial.println("Failed to start connection to SCD4X sensor!");
        while(true);
    }

    // Empty line for better readability.
    Serial.println();
}


void loop() {
    // Read out (=sample) all measured values from the sensor.
    if(sensor.sample(sensorSample) == true) {
        // Sampling successfully.
        Serial.print("CO2 (ppm)       :"); Serial.println(sensorSample.co2);
        Serial.print("Temperature (C) :"); Serial.println(sensorSample.temperature);
        Serial.print("Humidity (%)    :"); Serial.println(sensorSample.relativeHumidity);
    } else {
        // Error while sampling. Either sensor or interface error.
        Serial.println("Failed to sample sensor!");
    }

    // Just some empty lines for better readability.
    Serial.println();
    Serial.println();

    // Wait one second before sampling again.
    delay(1000);
}
