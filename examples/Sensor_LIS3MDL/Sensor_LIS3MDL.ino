/*
 * Minimum working example to control a LIS3MDL sensor using the
 * Ulm_Weatherballoon library.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_Weatherballoon.h"

// The pressure sensor
auto sensor = Ulm_LIS3MDL();

// A data sample from the pressure sensor
Ulm_LIS3MDL_Sample sensorSample = {};

/**
 * This function will run once at the beginning
 * and will initialise all interfaces and sensors.
 */
void setup() {
    // Start serial connection for message outputs.
    Serial.begin(115200);
    while(!Serial) {}

    // Start pressure sensor and check if it works.
    if(sensor.begin() == true) {
        // Pressure sensor works
        Serial.println("LIS3MDL sensor initialized successfully.");
    } else {
        // Pressure sensor or interface does NOT work!
        Serial.println("Failed to start connection to LIS3MDL sensor!");
        while(true);
    }

    // Empty line for better readability.
    Serial.println();
}


void loop() {
    // Read out (=sample) all measured values from the sensor.
    if(sensor.sample(sensorSample) == true) {
        // Sampling successfully.
        Serial.print("Magnet X (uT):"); Serial.println(sensorSample.magnetX);
        Serial.print("Magnet Y (uT):"); Serial.println(sensorSample.magnetY);
        Serial.print("Magnet Z (uT):"); Serial.println(sensorSample.magnetZ);
    } else {
        // Error while sampling. Either sensor or interface error.
        Serial.println("Failed to sample sensor!");
    }

    // Just some empty lines for better readability.
    Serial.println();
    Serial.println();

    // Wait one second before sampling again.
    delay(1000);
}
