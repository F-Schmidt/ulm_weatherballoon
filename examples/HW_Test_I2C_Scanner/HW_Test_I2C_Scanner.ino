/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. It is an I2C scanner that also outputs
 * the connected sensor type read from a LUT.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <Arduino.h>                // <- Import Arduino-functions
#include <Ulm_Weatherballoon.h>     // <- Import weather-balloon components

// --------------------------------------------------------------------------------------------------------------------
// String definition used for outputs for clear message structure.
// --------------------------------------------------------------------------------------------------------------------
const String SPACER = "=============================================================";

/**
 * Endless loop to terminate this program.
 */
void terminate() {
    while (true) {
        delay(10);
    }
}


/**
 * This function will run once at the beginning and will set up all
 * required interfaces and output some text to the user.
 */
void setup() {
    // This call will start the i2c interface of the Arduino.
    Wire.begin();

    // We will also start the serial connection and wait for it
    // to begin. After that, we will output a line of text, to
    // make sure, that Serial is running.
    Serial.begin(115200);
    while(!Serial) {}

    // Output information about this program.
    Serial.println(SPACER);
    Serial.println("[INFO] Running I2C scanner...");
    Serial.println(SPACER);
    Serial.println("PLEASE NOTE:");
    Serial.println("If this program does not continue, then the wiring might be");
    Serial.println("incorrect. The program should terminate within a few seconds.");
    Serial.println();
    Serial.println("All devices at the interface will be detected, but this");
    Serial.println("program only matches addresses to a few known sensors. It is");
    Serial.println("possible, that the detected sensor is not the one you have in");
    Serial.println("your hardware design!");
    Serial.println(SPACER);
}


/**
 * Run scan of I2C bus. Although the loop function is actually
 * executed several times (until power is gone), the scan will
 * only be performed once.
 */
void loop() {

    uint8_t amountDevices = 0;

    for(uint8_t address = 0x0B; address <= 0x77; address++) {
        // Try to talk to the device at 'address'. If it responds with an ACK,
        // then the Wire.endTransmission() will return 0.
        Wire.beginTransmission(address);
        if(Wire.endTransmission() == 0) {
            // I2C device found.
            amountDevices = amountDevices + 1;
            Serial.print("[INFO] Found device at address 0x");
            Serial.print(address, HEX);
            Serial.print(" - sensor type: ");

            // Try to match the detected address to known sensors. Only
            // sensors that may be used in our high altitude balloon
            // will be matched here!
            String detectedSensor;
            switch (address) {
                case 0x44:              detectedSensor = "SHT4X";   break;
                case 0x5C: case 0x5D:   detectedSensor = "LPS22H";  break;
                case 0x1D: case 0x53:   detectedSensor = "ADXL343"; break;
                case 0x1C: case 0x1E:   detectedSensor = "LIS3MDL"; break;
                case 0x62:              detectedSensor = "SCD4X";   break;
                case 0x68:              detectedSensor = "DS3231";  break;
                case 0x76:              detectedSensor = "BMP280";  break;
                case 0x77:              detectedSensor = "MS5611";  break;
                default:                detectedSensor = "UNKNOWN"; break;
            }
            Serial.println(detectedSensor);
        }
    }

    if(amountDevices == 0) {
        Serial.println("[FAIL] No devices connected via I2C!");
    }

    Serial.println(SPACER);
    Serial.println("[INFO] I2C scanner done. Press RESET button to run again.");
    Serial.println(SPACER);
    Serial.println();
    Serial.println();
    terminate();
}
