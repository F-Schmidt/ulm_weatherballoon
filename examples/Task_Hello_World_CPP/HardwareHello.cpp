//
// Created by Falko Schmidt on 05.09.22.
//

#include <Arduino.h>
#include "HardwareHello.h"

// This is the pin, that your LED is connected to.
#define             LED_HELLO_WORLD_PIN             (13u)

bool HardwareHello::begin() {
    // TODO Init LED pin and set it to be LOW
    return true;
}

[[noreturn]] void HardwareHello::sayHello() {
    // TODO Flash LED forever using this pattern: 500ms ON, 1000ms OFF
}
