//
// Created by Falko Schmidt on 05.09.22.
//

#ifndef UULM_SGUBERRY_PIO_TEMPLATE_SOFTWAREHELLO_H
#define UULM_SGUBERRY_PIO_TEMPLATE_SOFTWAREHELLO_H


#include "AbstractHello.h"

/**
 * This class implements 'Hello World' in the
 * same way, that software engineers use to
 * get started with a new system.
 * This means, that we will use the serial
 * interface of the microcontroller to print
 * some text.
 *
 * @author Falko Schmidt
 * @date 05. September 2022
 */
class SoftwareHello : public AbstractHello {
public:

    /**
     * Starts the serial interface.
     * @return always true, because there are no detectable errors.
     */
    bool begin() override;

    /**
     * Writes a string once to the serial interface.
     */
    void sayHello() override;
};


#endif //UULM_SGUBERRY_PIO_TEMPLATE_SOFTWAREHELLO_H
