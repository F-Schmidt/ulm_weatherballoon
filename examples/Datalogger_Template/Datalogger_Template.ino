#include <Arduino.h>
#include "Ulm_Weatherballoon.h"

#if !defined(PICO_RP2040)
#error Please select Arduino UNO R3 as board!
#endif

// --------------------------------------------------------------------------------------------------------------------
// Pins
// --------------------------------------------------------------------------------------------------------------------
constexpr uint8_t SD_CS_PIN = /*TODO your pin here*/;
constexpr uint8_t SD_DETECT_PIN = /*TODO your pin here*/;
// TODO Create all pin definitions right here.

// --------------------------------------------------------------------------------------------------------------------
// SENSORS
// --------------------------------------------------------------------------------------------------------------------
// TODO Create all your sensors here.

// --------------------------------------------------------------------------------------------------------------------
// STORAGE DEVICE
// --------------------------------------------------------------------------------------------------------------------
Ulm_SDStorage storage = ;// TODO Create Ulm_SDStorage to handle storage functions of SD card.


// ====================================================================================================================
// DATA REPRESENTATION
// ====================================================================================================================
/**
 * Structure representing our raw sensor data.
 * All of these values will be sampled and stored to SD card.
 */
struct SensorData {
    unsigned long uptimeMillis;     // [MCU]        Uptime of the MCU (time since power on) in milliseconds
    // TODO add all of your required sensor samples here.
} sensorData;


// ====================================================================================================================
// PROGRAM ROUTINE
// ====================================================================================================================
/**
 * Storage routine for sensor data. The data will be formatted and then written to
 * SD card. After that, the onboard LED will be either green (success) or red (failure).
 */
void storeSensorData() {
    // TODO Format sensor data into string and store that string to SD card.
}


/**
 * This function runs once and initializes all interfaces, sensors and actors of the implemented datalogger.
 * Also, a headline will be written into the datafile.
 */
void setup() {
    // TODO:
    //  Start all of your sensors calling begin() and checking the return type.
    //  Make sure, that you know how to handle errors. What would you like to do,
    //  if a sensor does not start up correctly?
}


/**
 * This function runs forever.
 * We will use it to implement our sampling and storage routine.
 */
void loop() {
    // Get the current uptime and sample all sensors.
    sensorData.uptimeMillis = millis();
    // TODO: Sample all sensors

    // TODO: Check for errors and display them

    // Format and store sampled data.
    storeSensorData();

    // TODO: Wait for some time
}
