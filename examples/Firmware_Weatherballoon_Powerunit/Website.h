//
// Created by Falko Alrik Schmidt on 03.05.24.
//

#ifndef WB_POWERUNIT_FIRMWARE_WEBSITE_H
#define WB_POWERUNIT_FIRMWARE_WEBSITE_H

inline auto WEBSITE_TEXT = R"html(
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Weatherballoon Powerunit - Overview</title>
    <style>
        :root {
            --FS_WHITE: #FFFFFF;
        }

        html, body {
            height: 100%;
            padding: 0;
            margin: 0;
        }

        body {
            background-color: var(--FS_WHITE);
        }

        div {
            border-radius: 20px;
        }

        svg {
            height: 100%;
            fill: var(--FS_WHITE);
        }

        .info_div {
            background: black;
            flex: 33.33%;
            height: 100%;
        }

        .icon_div {
            height: calc(20% - 10px);
            display: flex;
            justify-content: end;
            padding: 10px;
        }

        .description_label {
            height: calc(20% - 10pt);
            display: flex;
            padding: 10px;
            color: var(--FS_WHITE);
            text-align: left;
            width: 100%;
            align-items: center;
            font-family: sans-serif;
            font-weight: 600;
            font-size: 3vh;
        }

        .measurement_div {
            font-family: sans-serif;
            height: 80%;
            width: 100%;
            color: var(--FS_WHITE);
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .value_span {
            font-size: 100px;
            font-weight: 600;
            width: 60%;
            text-align: right;
        }

        .unit_span {
            font-size: 30px;
            width: 30%;
            text-align: left;
        }
    </style>
</head>
<body>
<div style="display: flex; flex-direction: column; height: calc(100% - 10px - 5% - 10px); gap: 10px; padding: 0 10px">
    <div style="display: flex; flex-direction: row; height: calc(50% - 10px); gap: 10px; padding-top: 10px">
        <!-- BATTERY VOLTAGE -->
        <div id="cell_voltage_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Batteriespannung</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path d="M349.4 44.6c5.9-13.7 1.5-29.7-10.6-38.5s-28.6-8-39.9 1.8l-256 224c-10 8.8-13.6 22.9-8.9 35.3S50.7 288 64 288H175.5L98.6 467.4c-5.9 13.7-1.5 29.7 10.6 38.5s28.6 8 39.9-1.8l256-224c10-8.8 13.6-22.9 8.9-35.3s-16.6-20.7-30-20.7H272.5L349.4 44.6z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <div style="display: block; align-items: baseline">
                    <span id="cell_voltage_value" class="value_span">0.00</span>
                    <span class="unit_span">V</span>
                </div>
            </div>
        </div>

        <!-- BATTERY PERCENTAGE -->
        <div id="cell_percentage_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Batterieladung</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path id="battery_percentage_icon" d="M80 160c-8.8 0-16 7.2-16 16V336c0 8.8 7.2 16 16 16H464c8.8 0 16-7.2 16-16V176c0-8.8-7.2-16-16-16H80zM0 176c0-44.2 35.8-80 80-80H464c44.2 0 80 35.8 80 80v16c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32v16c0 44.2-35.8 80-80 80H80c-44.2 0-80-35.8-80-80V176z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <div style="display: block; align-items: baseline">
                    <span id="cell_percentage_value" class="value_span">00</span>
                    <span class="unit_span">%</span>
                </div>
            </div>
        </div>

        <!-- BATTERY DISCHARGE RATE -->
        <div id="battery_charge_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Entladerate</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path id="battery_charge_icon" d="M96 0C78.3 0 64 14.3 64 32v96h64V32c0-17.7-14.3-32-32-32zM288 0c-17.7 0-32 14.3-32 32v96h64V32c0-17.7-14.3-32-32-32zM32 160c-17.7 0-32 14.3-32 32s14.3 32 32 32v32c0 77.4 55 142 128 156.8V480c0 17.7 14.3 32 32 32s32-14.3 32-32V412.8c12.3-2.5 24.1-6.4 35.1-11.5c-2.1-10.8-3.1-21.9-3.1-33.3c0-80.3 53.8-148 127.3-169.2c.5-2.2 .7-4.5 .7-6.8c0-17.7-14.3-32-32-32H32zM576 368a144 144 0 1 0 -288 0 144 144 0 1 0 288 0zm-64 0c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s7.2-16 16-16H496c8.8 0 16 7.2 16 16z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <div style="display: block; align-items: baseline">
                    <span id="battery_charge_value" class="value_span">-00.0</span>
                    <span class="unit_span">%/h</span>
                </div>
            </div>
        </div>
    </div>
    <div style="display: flex; flex-direction: row; height: calc(50% - 10px); gap: 10px; padding-bottom: 10px">
        <!-- SYSTEM TEMPERATURE -->
        <div id="temperature_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Innentemperatur der Batterieeinheit</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                    <path d="M160 64c-26.5 0-48 21.5-48 48V276.5c0 17.3-7.1 31.9-15.3 42.5C86.2 332.6 80 349.5 80 368c0 44.2 35.8 80 80 80s80-35.8 80-80c0-18.5-6.2-35.4-16.7-48.9c-8.2-10.6-15.3-25.2-15.3-42.5V112c0-26.5-21.5-48-48-48zM48 112C48 50.2 98.1 0 160 0s112 50.1 112 112V276.5c0 .1 .1 .3 .2 .6c.2 .6 .8 1.6 1.7 2.8c18.9 24.4 30.1 55 30.1 88.1c0 79.5-64.5 144-144 144S16 447.5 16 368c0-33.2 11.2-63.8 30.1-88.1c.9-1.2 1.5-2.2 1.7-2.8c.1-.3 .2-.5 .2-.6V112zM208 368c0 26.5-21.5 48-48 48s-48-21.5-48-48c0-20.9 13.4-38.7 32-45.3V208c0-8.8 7.2-16 16-16s16 7.2 16 16V322.7c18.6 6.6 32 24.4 32 45.3z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <div style="display: block; align-items: baseline">
                    <span id="temperature_value" class="value_span">00.0</span>
                    <span class="unit_span">°C</span>
                </div>
            </div>
        </div>

        <!-- Wi-Fi ACCESS POINT POWER-DOWN -->
        <div id="remaining_time_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Zeit bis WLAN-Shutdown</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                    <path d="M54.2 202.9C123.2 136.7 216.8 96 320 96s196.8 40.7 265.8 106.9c12.8 12.2 33 11.8 45.2-.9s11.8-33-.9-45.2C549.7 79.5 440.4 32 320 32S90.3 79.5 9.8 156.7C-2.9 169-3.3 189.2 8.9 202s32.5 13.2 45.2 .9zM320 256c56.8 0 108.6 21.1 148.2 56c13.3 11.7 33.5 10.4 45.2-2.8s10.4-33.5-2.8-45.2C459.8 219.2 393 192 320 192s-139.8 27.2-190.5 72c-13.3 11.7-14.5 31.9-2.8 45.2s31.9 14.5 45.2 2.8c39.5-34.9 91.3-56 148.2-56zm64 160a64 64 0 1 0 -128 0 64 64 0 1 0 128 0z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <div style="display: block; align-items: baseline">
                    <span id="remaining_time_value" class="value_span">0:00</span>
                    <span class="unit_span">h:min</span>
                </div>
            </div>
        </div>

        <!-- FLIGHT-READY / SYSTEM STATUS -->
        <div id="flight_status_div" class="info_div">
            <div class="icon_div">
                <p class="description_label">Flugstatus</p>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <path d="M176 24c0-13.3-10.7-24-24-24s-24 10.7-24 24V64c-35.3 0-64 28.7-64 64H24c-13.3 0-24 10.7-24 24s10.7 24 24 24H64v56H24c-13.3 0-24 10.7-24 24s10.7 24 24 24H64v56H24c-13.3 0-24 10.7-24 24s10.7 24 24 24H64c0 35.3 28.7 64 64 64v40c0 13.3 10.7 24 24 24s24-10.7 24-24V448h56v40c0 13.3 10.7 24 24 24s24-10.7 24-24V448h56v40c0 13.3 10.7 24 24 24s24-10.7 24-24V448c35.3 0 64-28.7 64-64h40c13.3 0 24-10.7 24-24s-10.7-24-24-24H448V280h40c13.3 0 24-10.7 24-24s-10.7-24-24-24H448V176h40c13.3 0 24-10.7 24-24s-10.7-24-24-24H448c0-35.3-28.7-64-64-64V24c0-13.3-10.7-24-24-24s-24 10.7-24 24V64H280V24c0-13.3-10.7-24-24-24s-24 10.7-24 24V64H176V24zM160 128H352c17.7 0 32 14.3 32 32V352c0 17.7-14.3 32-32 32H160c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32zm192 32H160V352H352V160z"/>
                </svg>
            </div>
            <div class="measurement_div">
                <svg width="50%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                    <path id="flight_status_icon" d="M256 0c-35 0-64 59.5-64 93.7v84.6L8.1 283.4c-5 2.8-8.1 8.2-8.1 13.9v65.5c0 10.6 10.2 18.3 20.4 15.4l171.6-49 0 70.9-57.6 43.2c-4 3-6.4 7.8-6.4 12.8v42c0 7.8 6.3 14 14 14c1.3 0 2.6-.2 3.9-.5L256 480l110.1 31.5c1.3 .4 2.6 .5 3.9 .5c6 0 11.1-3.7 13.1-9C344.5 470.7 320 422.2 320 368c0-60.6 30.6-114 77.1-145.6L320 178.3V93.7C320 59.5 292 0 256 0zM640 368a144 144 0 1 0 -288 0 144 144 0 1 0 288 0zm-76.7-43.3c6.2 6.2 6.2 16.4 0 22.6l-72 72c-6.2 6.2-16.4 6.2-22.6 0l-40-40c-6.2-6.2-6.2-16.4 0-22.6s16.4-6.2 22.6 0L480 385.4l60.7-60.7c6.2-6.2 16.4-6.2 22.6 0z"/>
                </svg>
            </div>
        </div>
    </div>
</div>
<div style="display: flex; flex-direction: row; align-items: center; justify-content: space-between; height: 5%; margin: 10px; background: black">
    <p style="width: 33.33%; font-size: calc(3vh); margin: 0; font-family: sans-serif; color: white; text-align: center;">
        Letztes Update: <span id="last_update_time_span">N/A</span>
    </p>
    <p style="width: 33.33%; font-size: calc(3vh); margin: 0; font-family: sans-serif; color: white; text-align: center;">
        Nächstes Update: <span id="update_time_span">N/A</span>
    </p>
</div>

</body>
<script type="application/javascript" language="JavaScript">
    // --- COLORS ---
    const COLOR_GRAY    = "#444444";
    const COLOR_RED     = "#990000";
    const COLOR_YELLOW  = "#DD7700";
    const COLOR_GREEN   = "#007700";
    // --- BATTERY LOGOS ---
    const BATTERY_EMPTY         = "M80 160c-8.8 0-16 7.2-16 16V336c0 8.8 7.2 16 16 16H464c8.8 0 16-7.2 16-16V176c0-8.8-7.2-16-16-16H80zM0 176c0-44.2 35.8-80 80-80H464c44.2 0 80 35.8 80 80v16c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32v16c0 44.2-35.8 80-80 80H80c-44.2 0-80-35.8-80-80V176z";
    const BATTERY_QUARTER       = "M464 160c8.8 0 16 7.2 16 16V336c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16H464zM80 96C35.8 96 0 131.8 0 176V336c0 44.2 35.8 80 80 80H464c44.2 0 80-35.8 80-80V320c17.7 0 32-14.3 32-32V224c0-17.7-14.3-32-32-32V176c0-44.2-35.8-80-80-80H80zm112 96H96V320h96V192z";
    const BATTERY_HALF          = "M464 160c8.8 0 16 7.2 16 16V336c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16H464zM80 96C35.8 96 0 131.8 0 176V336c0 44.2 35.8 80 80 80H464c44.2 0 80-35.8 80-80V320c17.7 0 32-14.3 32-32V224c0-17.7-14.3-32-32-32V176c0-44.2-35.8-80-80-80H80zm208 96H96V320H288V192z";
    const BATTERY_THREE_QUARTER = "M464 160c8.8 0 16 7.2 16 16V336c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16H464zM80 96C35.8 96 0 131.8 0 176V336c0 44.2 35.8 80 80 80H464c44.2 0 80-35.8 80-80V320c17.7 0 32-14.3 32-32V224c0-17.7-14.3-32-32-32V176c0-44.2-35.8-80-80-80H80zm272 96H96V320H352V192z";
    const BATTERY_FULL          = "M464 160c8.8 0 16 7.2 16 16V336c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16H464zM80 96C35.8 96 0 131.8 0 176V336c0 44.2 35.8 80 80 80H464c44.2 0 80-35.8 80-80V320c17.7 0 32-14.3 32-32V224c0-17.7-14.3-32-32-32V176c0-44.2-35.8-80-80-80H80zm368 96H96V320H448V192z";
    const BATTERY_ICONS         = [BATTERY_EMPTY, BATTERY_QUARTER, BATTERY_HALF, BATTERY_THREE_QUARTER, BATTERY_FULL];
    // --- CHARGER LOGOS ---
    const CHARGER_PLUS      = "M96 0C78.3 0 64 14.3 64 32v96h64V32c0-17.7-14.3-32-32-32zM288 0c-17.7 0-32 14.3-32 32v96h64V32c0-17.7-14.3-32-32-32zM32 160c-17.7 0-32 14.3-32 32s14.3 32 32 32v32c0 77.4 55 142 128 156.8V480c0 17.7 14.3 32 32 32s32-14.3 32-32V412.8c12.3-2.5 24.1-6.4 35.1-11.5c-2.1-10.8-3.1-21.9-3.1-33.3c0-80.3 53.8-148 127.3-169.2c.5-2.2 .7-4.5 .7-6.8c0-17.7-14.3-32-32-32H32zM432 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm16-208v48h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H448v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V384H368c-8.8 0-16-7.2-16-16s7.2-16 16-16h48V304c0-8.8 7.2-16 16-16s16 7.2 16 16z";
    const CHARGER_MINUS     = "M96 0C78.3 0 64 14.3 64 32v96h64V32c0-17.7-14.3-32-32-32zM288 0c-17.7 0-32 14.3-32 32v96h64V32c0-17.7-14.3-32-32-32zM32 160c-17.7 0-32 14.3-32 32s14.3 32 32 32v32c0 77.4 55 142 128 156.8V480c0 17.7 14.3 32 32 32s32-14.3 32-32V412.8c12.3-2.5 24.1-6.4 35.1-11.5c-2.1-10.8-3.1-21.9-3.1-33.3c0-80.3 53.8-148 127.3-169.2c.5-2.2 .7-4.5 .7-6.8c0-17.7-14.3-32-32-32H32zM576 368a144 144 0 1 0 -288 0 144 144 0 1 0 288 0zm-64 0c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16s7.2-16 16-16H496c8.8 0 16 7.2 16 16z";
    const CHARGER_ERROR     = "M96 0C78.3 0 64 14.3 64 32v96h64V32c0-17.7-14.3-32-32-32zM288 0c-17.7 0-32 14.3-32 32v96h64V32c0-17.7-14.3-32-32-32zM32 160c-17.7 0-32 14.3-32 32s14.3 32 32 32v32c0 77.4 55 142 128 156.8V480c0 17.7 14.3 32 32 32s32-14.3 32-32V412.8c12.3-2.5 24.1-6.4 35.1-11.5c-2.1-10.8-3.1-21.9-3.1-33.3c0-80.3 53.8-148 127.3-169.2c.5-2.2 .7-4.5 .7-6.8c0-17.7-14.3-32-32-32H32zM432 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm0-96a24 24 0 1 1 0 48 24 24 0 1 1 0-48zm0-144c8.8 0 16 7.2 16 16v80c0 8.8-7.2 16-16 16s-16-7.2-16-16V288c0-8.8 7.2-16 16-16z";
    const CHARGER_CHARGING  = "M96 0C78.3 0 64 14.3 64 32v96h64V32c0-17.7-14.3-32-32-32zM288 0c-17.7 0-32 14.3-32 32v96h64V32c0-17.7-14.3-32-32-32zM32 160c-17.7 0-32 14.3-32 32s14.3 32 32 32v32c0 77.4 55 142 128 156.8V480c0 17.7 14.3 32 32 32s32-14.3 32-32V412.8c12.3-2.5 24.1-6.4 35.1-11.5c-2.1-10.8-3.1-21.9-3.1-33.3c0-80.3 53.8-148 127.3-169.2c.5-2.2 .7-4.5 .7-6.8c0-17.7-14.3-32-32-32H32zM432 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm47.9-225c4.3 3.7 5.4 9.9 2.6 14.9L452.4 356H488c5.2 0 9.8 3.3 11.4 8.2s-.1 10.3-4.2 13.4l-96 72c-4.5 3.4-10.8 3.2-15.1-.6s-5.4-9.9-2.6-14.9L411.6 380H376c-5.2 0-9.8-3.3-11.4-8.2s.1-10.3 4.2-13.4l96-72c4.5-3.4 10.8-3.2 15.1 .6z";
    // --- PLANE LOGOS ---
    const FLIGHT_READY  = "M256 0c-35 0-64 59.5-64 93.7v84.6L8.1 283.4c-5 2.8-8.1 8.2-8.1 13.9v65.5c0 10.6 10.2 18.3 20.4 15.4l171.6-49 0 70.9-57.6 43.2c-4 3-6.4 7.8-6.4 12.8v42c0 7.8 6.3 14 14 14c1.3 0 2.6-.2 3.9-.5L256 480l110.1 31.5c1.3 .4 2.6 .5 3.9 .5c6 0 11.1-3.7 13.1-9C344.5 470.7 320 422.2 320 368c0-60.6 30.6-114 77.1-145.6L320 178.3V93.7C320 59.5 292 0 256 0zM640 368a144 144 0 1 0 -288 0 144 144 0 1 0 288 0zm-76.7-43.3c6.2 6.2 6.2 16.4 0 22.6l-72 72c-6.2 6.2-16.4 6.2-22.6 0l-40-40c-6.2-6.2-6.2-16.4 0-22.6s16.4-6.2 22.6 0L480 385.4l60.7-60.7c6.2-6.2 16.4-6.2 22.6 0z";
    const FLIGHT_WARN   = "M256 0c-35 0-64 59.5-64 93.7v84.6L8.1 283.4c-5 2.8-8.1 8.2-8.1 13.9v65.5c0 10.6 10.2 18.3 20.4 15.4l171.6-49 0 70.9-57.6 43.2c-4 3-6.4 7.8-6.4 12.8v42c0 7.8 6.3 14 14 14c1.3 0 2.6-.2 3.9-.5L256 480l110.1 31.5c1.3 .4 2.6 .5 3.9 .5c6 0 11.1-3.7 13.1-9C344.5 470.7 320 422.2 320 368c0-60.6 30.6-114 77.1-145.6L320 178.3V93.7C320 59.5 292 0 256 0zM496 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm0-96a24 24 0 1 1 0 48 24 24 0 1 1 0-48zm0-144c8.8 0 16 7.2 16 16v80c0 8.8-7.2 16-16 16s-16-7.2-16-16V288c0-8.8 7.2-16 16-16z";
    const FLIGHT_FAIL   = "M256 0c-35 0-64 59.5-64 93.7v84.6L8.1 283.4c-5 2.8-8.1 8.2-8.1 13.9v65.5c0 10.6 10.2 18.3 20.4 15.4l171.6-49 0 70.9-57.6 43.2c-4 3-6.4 7.8-6.4 12.8v42c0 7.8 6.3 14 14 14c1.3 0 2.6-.2 3.9-.5L256 480l110.1 31.5c1.3 .4 2.6 .5 3.9 .5c6 0 11.1-3.7 13.1-9C344.5 470.7 320 422.2 320 368c0-60.6 30.6-114 77.1-145.6L320 178.3V93.7C320 59.5 292 0 256 0zM496 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm59.3-180.7L518.6 368l36.7 36.7c6.2 6.2 6.2 16.4 0 22.6s-16.4 6.2-22.6 0L496 390.6l-36.7 36.7c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6L473.4 368l-36.7-36.7c-6.2-6.2-6.2-16.4 0-22.6s16.4-6.2 22.6 0L496 345.4l36.7-36.7c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z";
    // --- HTML ELEMENTS ---
    const CELL_VOLTAGE_DIV      = document.getElementById("cell_voltage_div");
    const CELL_VOLTAGE_VALUE    = document.getElementById("cell_voltage_value");
    // ---
    const CELL_PERCENTAGE_DIV   = document.getElementById("cell_percentage_div");
    const CELL_PERCENTAGE_VALUE = document.getElementById("cell_percentage_value");
    const PERCENTAGE_ICON       = document.getElementById("battery_percentage_icon");
    // ---
    const BATTERY_CHARGE_DIV    = document.getElementById("battery_charge_div");
    const BATTERY_CHARGE_VALUE  = document.getElementById("battery_charge_value");
    const BATTERY_CHARGE_ICON   = document.getElementById("battery_charge_icon");
    // ---
    const TEMPERATURE_DIV       = document.getElementById("temperature_div");
    const TEMPERATURE_VALUE     = document.getElementById("temperature_value");
    // ---
    const HOTSPOT_UPTIME_DIV    = document.getElementById("remaining_time_div");
    const HOTSPOT_UPTIME_VALUE  = document.getElementById("remaining_time_value");
    // ---
    const FLIGHT_STATUS_DIV     = document.getElementById("flight_status_div");
    const FLIGHT_STATUS_ICON    = document.getElementById("flight_status_icon");
    // --- JAVASCRIPT VARIABLES ---
    let LAST_UPDATE_SECONDS = 0;

    function updateData() {
        // Send AJAX request to server and get JSON response.
        fetch('/data_update').then(response => response.json().catch(() => null))
            .then(data => {
                if (!data) {
                    return;
                }

                const {
                    hasBatteryMonitorError,
                    hasTemperatureSensorError,
                    cellVoltage,
                    cellPercent,
                    chargeRate,
                    isCharging,
                    hasChargerError,
                    temperature,
                    remainingMillis
                } = data;

                // These values will be used to calculate the flight ready status.
                let hasErrors = false;
                let hasWarnings = false;

                // =====================================================================================================
                // Update battery cell voltage field.
                // =====================================================================================================
                if(hasBatteryMonitorError || cellVoltage == null) {
                    CELL_VOLTAGE_DIV.style.backgroundColor = COLOR_GRAY;
                    CELL_VOLTAGE_VALUE.textContent = "N/A";
                } else {
                    // Update overall error and warning status.
                    hasErrors   |= cellVoltage > 4.2 || cellVoltage <= 3.0;
                    hasWarnings |= cellVoltage > 3.0 && cellVoltage <= 3.7;
                    // Update background color.
                    if (cellVoltage > 4.2) {
                        CELL_VOLTAGE_DIV.style.backgroundColor = COLOR_RED;
                    } else if (cellVoltage > 3.7) {
                        CELL_VOLTAGE_DIV.style.backgroundColor = COLOR_GREEN;
                    } else if (cellVoltage > 3.0) {
                        CELL_VOLTAGE_DIV.style.backgroundColor = COLOR_YELLOW;
                    } else {
                        CELL_VOLTAGE_DIV.style.backgroundColor = COLOR_RED;
                    }
                    CELL_VOLTAGE_VALUE.textContent = cellVoltage.toFixed(2);
                }
                // =====================================================================================================
                // Update battery percentage field.
                // =====================================================================================================
                if (hasBatteryMonitorError || cellPercent == null) {
                    CELL_PERCENTAGE_DIV.style.backgroundColor = COLOR_GRAY;
                    CELL_VOLTAGE_VALUE.textContent = "N/A";
                } else {
                    // Update overall error and warning status.
                    hasWarnings |= cellPercent > 50 && cellPercent <= 80;
                    hasErrors   |= cellPercent <= 50;
                    // Update background color.
                    if (cellPercent > 80) {
                        CELL_PERCENTAGE_DIV.style.backgroundColor = COLOR_GREEN;
                    } else if (cellPercent > 50) {
                        CELL_PERCENTAGE_DIV.style.backgroundColor = COLOR_YELLOW;
                    } else {
                        CELL_PERCENTAGE_DIV.style.backgroundColor = COLOR_RED;
                    }
                    // Update icon.
                    const index = Math.min(Math.floor(cellPercent / 25), BATTERY_ICONS.length - 1);
                    PERCENTAGE_ICON.setAttribute("d", BATTERY_ICONS[index]);
                    CELL_PERCENTAGE_VALUE.textContent = cellPercent.toFixed(0);
                }
                // =====================================================================================================
                // Update battery charge/discharge rate field.
                // =====================================================================================================
                if (hasBatteryMonitorError) {
                    BATTERY_CHARGE_DIV.style.backgroundColor = COLOR_GRAY;
                    BATTERY_CHARGE_VALUE.textContent = "N/A";
                } else {
                    // Update overall error and warning status.
                    hasErrors   |= hasChargerError || Math.abs(chargeRate) > 20;
                    hasWarnings |= Math.abs(chargeRate) > 15;
                    // Update background color.
                    if(hasChargerError || Math.abs(chargeRate) > 20) {
                        BATTERY_CHARGE_DIV.style.backgroundColor = COLOR_RED;
                    } else if (Math.abs(chargeRate) > 15) {
                        BATTERY_CHARGE_DIV.style.backgroundColor = COLOR_YELLOW;
                    } else {
                        BATTERY_CHARGE_DIV.style.backgroundColor = COLOR_GREEN;
                    }
                    // Update icon.
                    if(isCharging) {
                        BATTERY_CHARGE_ICON.setAttribute("d", CHARGER_CHARGING);
                    } else {
                        BATTERY_CHARGE_ICON.setAttribute("d", chargeRate > 0 ? CHARGER_PLUS : CHARGER_MINUS);
                    }
                    BATTERY_CHARGE_VALUE.textContent = chargeRate.toFixed(1);
                }
                // =====================================================================================================
                // Update temperature.
                // =====================================================================================================
                if (hasTemperatureSensorError) {
                    TEMPERATURE_DIV.style.backgroundColor = COLOR_GRAY;
                    TEMPERATURE_VALUE.textContent = "N/A";
                } else {
                    // Update overall error and warning status.
                    hasErrors   |= temperature > 60;
                    hasWarnings |= temperature > 40;
                    // Update background color.
                    TEMPERATURE_DIV.style.backgroundColor =
                        temperature > 60 ? COLOR_RED :
                        temperature > 40 ? COLOR_YELLOW :
                        COLOR_GREEN;
                    TEMPERATURE_VALUE.textContent = temperature.toFixed(1);
                }
                // =====================================================================================================
                // Update remaining time
                // =====================================================================================================
                // Update color.
                if(remainingMillis <= 2 * 60 * 1000) {
                    HOTSPOT_UPTIME_DIV.style.backgroundColor = COLOR_RED;
                } else if(remainingMillis <= 10 * 60 * 1000) {
                    HOTSPOT_UPTIME_DIV.style.backgroundColor = COLOR_YELLOW;
                } else {
                    HOTSPOT_UPTIME_DIV.style.backgroundColor = COLOR_GREEN;
                }
                const remainingHours = Math.floor(remainingMillis / (1000 * 60 * 60));
                const remainingMinutes = Math.floor((remainingMillis / (1000 * 60)) % 60);
                HOTSPOT_UPTIME_VALUE.textContent = [remainingHours, remainingMinutes]
                    .map(n => String(n).padStart(2, '0')).join(':');
                // =====================================================================================================
                // Update flight status
                // =====================================================================================================
                FLIGHT_STATUS_ICON.setAttribute("d",
                    hasErrors ? FLIGHT_FAIL : hasWarnings ? FLIGHT_WARN : FLIGHT_READY
                );
                FLIGHT_STATUS_DIV.style.backgroundColor =
                    hasErrors ? COLOR_RED : hasWarnings ? COLOR_YELLOW : COLOR_GREEN;

                // Reset the counter for last updated.
                LAST_UPDATE_SECONDS = 0;
            })
            .catch(
                // Currently nothing will be done on error.
            );
    }

    window.onload = () => {
        // Immediately update data when the page loads.
        updateData();

        // Start the countdown for subsequent updates.
        let count = 9;
        const updateSpan = document.getElementById("update_time_span");
        setInterval(() => {
            updateSpan.textContent = `in ${count} Sekunden.`;
            if (--count < 0) {
                updateData();
                count = 9;
            }
        }, 1000);

        // Timer to display the time since page load in HH:MM:SS.
        const timerElement = document.getElementById("last_update_time_span");
        setInterval(() => {
            LAST_UPDATE_SECONDS++;
            const hours   = Math.floor(LAST_UPDATE_SECONDS / 3600);
            const minutes = Math.floor((LAST_UPDATE_SECONDS % 3600) / 60);
            const secs    = LAST_UPDATE_SECONDS % 60;
            timerElement.textContent = [hours, minutes, secs]
                .map(n => String(n).padStart(2, '0'))
                .join(':');
        }, 1000);
    };

</script>
</html>
)html";

#endif //WB_POWERUNIT_FIRMWARE_WEBSITE_H
