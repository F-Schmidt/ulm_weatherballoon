/*
 * This software is designed to run in the weatherballoon power unit. Do not
 * use this for any other device or architecture.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <ArduinoJson.h>
#include <Arduino.h>
#include <WiFi.h>
#include <Wire.h>
#include "Ulm_Weatherballoon.h"
#include "Website.h"


// =========================================================================================================
// PIN DEFINITIONS
// =========================================================================================================
constexpr uint8_t I2C0_SDA                = 20;
constexpr uint8_t I2C0_SCL                = 21;

constexpr uint8_t MAX17048_PIN_ALERT      = 19;
constexpr uint8_t MCP73833_PIN_STAT1      = 16;
constexpr uint8_t MCP73833_PIN_STAT2      = 17;
constexpr uint8_t MCP73833_PIN_PG         = 18;

constexpr uint8_t TEMP_SENSOR_PINS[3]     = {4, 15, 22};


// =========================================================================================================
// WIFI SETTINGS
// =========================================================================================================
constexpr auto WIFI_AP_SSID                 = "FS_UULM_Weatherballoon_Powerunit";
constexpr auto WIFI_AP_PASSWORD             = "Weatherballoon_P0werun1t_AP_FS_2025";
constexpr unsigned long WIFI_AP_TIMEOUT     = 90 * 60 * 1000;   // 1h 30min (in ms)
const auto WIFI_SERVER_ADDRESS              = IPAddress(192, 168, 42, 1);


// =========================================================================================================
// SENSORS
// =========================================================================================================
auto BATTERY_MONITOR = Ulm_MAX17048(Wire, MAX17048_PIN_ALERT);
auto BATTERY_CHARGER = Ulm_MCP73833(MCP73833_PIN_STAT1, MCP73833_PIN_STAT2, MCP73833_PIN_PG);
Ulm_DS18B20 TEMPERATURE_SENSORS[3] = {
        Ulm_DS18B20(TEMP_SENSOR_PINS[0]),
        Ulm_DS18B20(TEMP_SENSOR_PINS[1]),
        Ulm_DS18B20(TEMP_SENSOR_PINS[2])
};


// =========================================================================================================
// ACTORS
// =========================================================================================================
auto LED = Ulm_LED_BuiltIn();


// =========================================================================================================
// WIFI RELATED COMPONENTS
// =========================================================================================================
unsigned long WEBSERVER_START_MILLIS = 0;
WiFiServer WEBSERVER(80);


// =========================================================================================================
// OPTIONAL SERIAL DEBUGGING
// =========================================================================================================
auto DEBUGGER = Ulm_SerialDebugger(Serial);


// =========================================================================================================
// All data that will be read is stored in this single struct.
// =========================================================================================================
struct {
    unsigned long uptimeMillis                  = 0;
    Ulm_MAX17048_Sample batteryMonitorSample    = {};
    Ulm_MCP73833_Sample batteryChargerSample    = {};
    float temperature                           = 0.0f;
} SensorData;


/**
 * In case of an error, turn the LED on and off in a fast sequence.
 * This will run forever and therefore halt the program.
 */
void errorHalt() {
    while (true) {
        LED.off();
        delay(100);
        LED.on();
        delay(100);
    }
}


/**
 * Stop all interfaces to reduce power consumption and
 * only blink an LED sometimes.
 */
void terminate() {
    WEBSERVER.end();
    WiFi.end();
    DEBUGGER.end();
    while (true) {
        LED.off();
        delay(1900);
        LED.on();
        delay(100);
    }
}


/**
 * This function will be executed ONCE at program start and it inits
 * all sensors, sets up all required pins and starts the Wi-Fi access
 * point.
 */
void setup() {
    // ===================================================================
    // Start the on-board LED.
    // ===================================================================
    LED.begin();

    // ===================================================================
    // Start debugger, if debugging mode is active
    // ===================================================================
    DEBUGGER.begin();

    // ===================================================================
    // Give the USB interface some time to settle.
    // ===================================================================
    for (uint8_t i = 0; i < 5; i++) {
        LED.on();
        delay(500);
        LED.off();
        delay(500);
    }
    LED.on();

    DEBUGGER.info("Starting power unit firmware with debugging outputs...");

    // ===================================================================
    // Configure the I2C pins to the custom board
    // layout. Only after this, Wire.begin() should be called!
    // ===================================================================
    if(Wire.setSCL(I2C0_SCL)) {
        DEBUGGER.debugf("Configured SCL of I2C0 to GPIO %d.", I2C0_SCL);
    } else {
        DEBUGGER.errorf("Failed to configure SCL of I2C0 to GPIO %u. %s", I2C0_SCL,
            "Please check the datasheet and use valid SCL pins for I2C0 only!");
        errorHalt();
    }

    if(Wire.setSDA(I2C0_SDA)) {
        DEBUGGER.debugf("Configured SDA of I2C0 to GPIO %u.", I2C0_SDA);
    } else {
        DEBUGGER.errorf("Failed to configure SDA of I2C0 to GPIO %u. %s", I2C0_SDA,
            "Please check the datasheet and use valid SDA pins for I2C0 only!");
        errorHalt();
    }

    // ===================================================================
    // Start battery monitoring devices.
    // ===================================================================
    if(BATTERY_MONITOR.begin()) {
        DEBUGGER.info("Successfully started battery monitor MAX17048 at I2C0.");
    } else {
        DEBUGGER.error("Failed to start battery monitor MAX17048 at I2C0!");
        errorHalt();
    }

    if(BATTERY_CHARGER.begin()) {
        DEBUGGER.info("Successfully started battery charger MCP73833 at GPIOs.");
    } else {
        DEBUGGER.error("Failed to start battery charger MCP73833 via GPIOs!");
        errorHalt();
    }

    // ===================================================================
    // Init all temperature sensors.
    // ===================================================================
    uint8_t initedSensors = 0;
    for(auto index = 0; index < std::size(TEMPERATURE_SENSORS); index++) {
        if(TEMPERATURE_SENSORS[index].begin()) {
            initedSensors++;
            DEBUGGER.infof("Started temperature sensor at pin %d.", TEMP_SENSOR_PINS[index]);
        } else {
            DEBUGGER.errorf("Failed to start temperature sensor at pin %d!", TEMP_SENSOR_PINS[index]);
        }
    }
    if(initedSensors == std::size(TEMPERATURE_SENSORS)) {
        DEBUGGER.info("All temperature sensors started successfully.");
    } else if(initedSensors == 0) {
        DEBUGGER.error("All temperature sensors failed to init. Please check hardware!");
    } else {
        DEBUGGER.warnf("Only %d/%d temperature sensors started successfully!",
            initedSensors, std::size(TEMPERATURE_SENSORS));
    }

    // ===================================================================
    // Next up: WiFi. Check for module first, after that start
    // the access point.
    // ===================================================================
    if (WiFi.status() != WL_NO_MODULE) {
        DEBUGGER.info("Successfully checked WiFi-module connectivity.");
    } else {
        DEBUGGER.error("Failed to check WiFi-module connection!");
        errorHalt();
    }


    WiFi.config(WIFI_SERVER_ADDRESS);
    if (WiFi.beginAP(WIFI_AP_SSID, WIFI_AP_PASSWORD) == WL_CONNECTED) {
        DEBUGGER.infof("Successfully started WiFi access point with name %s.", WIFI_AP_SSID);
    } else {
        DEBUGGER.error("Failed to start WiFi access point!");
        errorHalt();
    }

    // ===================================================================
    // Start webserver.
    // ===================================================================
    WEBSERVER.begin();
    WEBSERVER_START_MILLIS = millis();
}


/**
 * Reads out all temperature sensors that were properly
 * started. Sensors, that return invalid values will
 * be ignored.
 * @return the current temperature inside the power unit.
 */
float sampleTemperature() {
    Ulm_DS18B20_Sample sample = {};
    float sum = 0;
    uint8_t sensors = 0;
    for(auto &sensor : TEMPERATURE_SENSORS) {
        if(sensor.sample(sample)) {
            sum += sample.temperature;
            sensors++;
        }
    }

    // If no sensor is active, then return the internal CPU temperature. Otherwise,
    // the external sensors will be used for a more accurate reading.
    return sensors == 0 ? analogReadTemp(3.3f) : sum / static_cast<float>(sensors);
}



void handleDataUpdateRequest(WiFiClient & client) {
    // Fetch sensor data.
    // Inject values in this order:
    //      (1) cellVoltage             FLOAT
    //      (2) cellPercent             FLOAT
    //      (3) chargeRate              FLOAT
    //      (4) battery charging        BOOL
    //      (5) battery charger error   BOOL
    //      (6) temperature             FLOAT
    //      (7) remainingMillis         UNSIGNED LONG
    auto data = JsonDocument();
    data["cellVoltage"] = SensorData.batteryMonitorSample.cellVoltage;
    data["cellPercent"] = SensorData.batteryMonitorSample.cellPercent;
    data["chargeRate"] = SensorData.batteryMonitorSample.chargeRate;
    data["isBatteryCharging"] = SensorData.batteryChargerSample.status == CHARGING;
    data["hasBatteryChargingError"] = SensorData.batteryChargerSample.error,
    data["temperature"] = SensorData.temperature,
    data["remainingMillis"] = max(0, WIFI_AP_TIMEOUT  + WEBSERVER_START_MILLIS - SensorData.uptimeMillis);

    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: application/json");
    client.println("Connection: close");
    client.println();

    String jsonResult;
    serializeJson(data, jsonResult);
    client.print(jsonResult);
}



void handleRootRequest(WiFiClient & client) {
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println("Connection: close");
    client.println();
    client.print(WEBSITE_TEXT);
}



/**
 * This function will run forever after execution of setup().
 * In this program, we will do three tasks:
 *      (1) Sample sensors every second and temporarily store the data.
 *      (2) Send HTML-code with sensor values to a connected client.
 *      (3) Check if certain amount of time has passed and in this
 *          case turn of the AP and terminate.
 */
void loop() {
    // ===================================================================
    // Sample data every second and not on client request.
    // ===================================================================
    if(millis() - SensorData.uptimeMillis > 1000) {
        SensorData.uptimeMillis = millis();
        BATTERY_MONITOR.sample(SensorData.batteryMonitorSample);
        BATTERY_CHARGER.sample(SensorData.batteryChargerSample);
        SensorData.temperature = sampleTemperature();
    }

    // ===================================================================
    // Check if client is waiting for a response. If so, then the
    // sensors will be read out and sent to client. Otherwise, the
    // microcontroller is just waiting for a client.
    // ===================================================================
    if (WiFiClient client = WEBSERVER.accept()) {
        // HTTP request ends with a blank line.
        //bool currentLineIsBlank = true;
        auto path = String();
        while (client.connected()) {
            if (client.available()) {
                String line = client.readStringUntil('\n');
                line.trim();
                if (line.length() == 0) {
                    DEBUGGER.debugf("Client requested path: %s", path);
                    // Empty line -> end of HTTP request.
                    //  Response must be sent to client now depending on the path.
                    if (path.equals("/") || path.length() == 0) {
                        handleRootRequest(client);
                        client.flush();
                        client.stop();
                        break;
                    }
                    if (path.equals("/data_update")) {
                        handleDataUpdateRequest(client);
                        client.flush();
                        client.stop();
                        break;
                    }
                } else if (line.startsWith("GET")) {
                    const int start = line.indexOf(' ') + 1;
                    if (const int end = line.indexOf(' ', start); start > 0 && end > start) {
                        // Path found -> store it in variable.
                        path = line.substring(start, end);
                        path.replace(WIFI_SERVER_ADDRESS.toString(), "");
                    } else {
                        // No valid path found -> just set root as default.
                        path = "/";
                    }
                }
            }
        }

        // Close connection to client.
        client.stop();
    }

    // ===================================================================
    // Please note: To reduce the power required, the access point
    // will turn off after a specific amount of time.
    // ===================================================================
    if(WIFI_AP_TIMEOUT < millis() - WEBSERVER_START_MILLIS) {
        terminate();
    }
}
