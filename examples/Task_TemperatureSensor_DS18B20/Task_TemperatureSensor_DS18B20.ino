#include <Arduino.h>                // <- Import Arduino-functions
#include <Ulm_Weatherballoon.h>     // <- Import weather-balloon components

// This can be used to control the temperature sensor on your PCB.
// Please note, that you need to adapt the pin according to your layout.
constexpr uint8_t MY_TEMP_SENSOR_PIN = /*TODO*/;
Ulm_DS18B20 temperatureSensor = Ulm_DS18B20(MY_TEMP_SENSOR_PIN);

void setup() {
    // TODO Your task here:
    //  (1) Start serial connection to PC with a baud of 9600.
    //  (2) Initialize the sensor.
    //  (3a) If the sensor is inited successfully, then continue normally.
    //       You may output a message to Serial, if you so wish.
    //  (3b) If the sensor fails, to init, think what you can do? Does it make sense to continue
    //       with the program, or do you stop? What, if a sensor fails on the start of the weather balloon?
}


void loop() {
    // TODO Your task here:
    //  (1) Read out the temperature sensor.
    //  (2) Write the sensor value to Serial to output it.
    //  (3) Repeat with a delay of one second.
    //  -------------------------------------------------------------------------
    //  Optional:
    //  The built-in temperature has a maximum of three digital points.
    //  How can you output a float with only three digits?
    //  -------------------------------------------------------------------------
    //  Optional:
    //  Upgrade your sensor values! Reading a sensor and storing each value makes
    //  it sensible to reading errors, which result in either higher or lower
    //  read values than normal. Reading more reliable values can be easily
    //  implemented by reading multiple sensor values one after the other
    //  and calculate the median value.
}
