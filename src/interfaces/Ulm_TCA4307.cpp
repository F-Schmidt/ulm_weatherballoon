/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_TCA4307.h"

Ulm_TCA4307::Ulm_TCA4307(const uint8_t enablePin, const int8_t readyPin) : enablePin(enablePin), readyPin(readyPin) {
    // Nothing to do here.
}


Ulm_TCA4307::~Ulm_TCA4307() = default;


bool Ulm_TCA4307::begin() {
    // ENABLE pin will be configured to be OUTPUT of MCU.
    pinMode(this->enablePin, OUTPUT);

    // Check if there is a READY pin. If not, then
    // we are done here...
    if(!this->hasReadyPin()) {
        return true;
    }
    // ...otherwise the ready pin will be set up as INPUT with
    // active pull-up resistor. The ready pin will be pulled low,
    // when device is ready.
    pinMode(this->readyPin, INPUT_PULLUP);

    // Finally wake up device and check, if it is running.
    return this->wake();
}


const char * Ulm_TCA4307::getDeviceName() const {
    return "TCA4307 I2C buffer";
}


bool Ulm_TCA4307::hasReadyPin() const {
    return this->readyPin >= 0;
}


bool Ulm_TCA4307::sleep(const uint16_t timeoutMillis) const {
    digitalWrite(this->enablePin, LOW);
    return checkReadyPinStatusWithinTimeout(LOW, timeoutMillis);
}


bool Ulm_TCA4307::wake(const uint16_t timeoutMillis) const {
    digitalWrite(this->enablePin, HIGH);
    return checkReadyPinStatusWithinTimeout(HIGH, timeoutMillis);
}


bool Ulm_TCA4307::checkReadyPinStatusWithinTimeout(const uint8_t expectedStatus, const uint16_t timeoutMillis) const {
    if(!this->hasReadyPin()) {
        // No ready pin -> we are not able to check.
        return true;
    }

    const unsigned long start = millis();
    while (millis() - start < timeoutMillis) {
        if(digitalRead(this->readyPin) == expectedStatus) {
            return true;
        }
    }
    return false;
}
