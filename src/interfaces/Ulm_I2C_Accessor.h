//
// Created by Falko Alrik Schmidt on 15.05.24.
//

#ifndef ULM_WEATHERBALLOON_ULM_I2C_ACCESSOR_H
#define ULM_WEATHERBALLOON_ULM_I2C_ACCESSOR_H


#include <Arduino.h>
#include <Wire.h>
#include <unordered_set>

class Ulm_I2C_Accessor {

private:
    std::unordered_set<const TwoWire*> INITIALISED_I2C_INTERFACES = {};

protected:
    uint8_t address;
    TwoWire& i2c;

public:
    explicit Ulm_I2C_Accessor(uint8_t address, TwoWire &i2c = Wire);

protected:
    bool startInterface();

    /**
     * Generic write method to send data via i2c.
     * @param buf the buffer to send.
     * @param len the length of buf.
     * @param stopBit whether to send a stopBit (true) or not (false).
     * @return {@code true} on success, {@code false} if there was any error.
     */
    bool write(const uint8_t* buf, size_t len, bool stopBit = true) const;
    [[nodiscard]] bool write8(uint8_t val, bool stopBit = true) const; // TODO documentation. And keep it or remove?

    bool read(uint8_t* buf, size_t len, bool stopBit = true) const;
    bool read8(uint8_t* val, bool stopBit = true) const;
    bool read16(uint16_t* val, bool stopBit = true) const;
    bool read24(uint32_t* val, bool stopBit = true) const;
    bool read32(uint32_t* val, bool stopBit = true) const;

    bool writeThenRead(const uint8_t* sendBuf, size_t sendLen, uint8_t* recvBuf, size_t recvLen, bool stopBit) const;


};


#endif //ULM_WEATHERBALLOON_ULM_I2C_ACCESSOR_H
