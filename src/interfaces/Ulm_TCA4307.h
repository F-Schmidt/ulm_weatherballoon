/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_TCA4307_H
#define ULM_WEATHERBALLOON_ULM_TCA4307_H


#include <Arduino.h>
#include "Ulm_Device.h"


/**
 * This class allows to control i2c buffers of type TCA4307.
 * These can be handy to allow hot-pluggable i2c connections with
 * stuck bus recovery.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_TCA4307 final : public Ulm_Device {


private:
    /**
     * The GPIO, which the buffer ENABLE pin is connected to.
     */
    uint8_t enablePin;

    /**
     * The GPIO, which the buffer READY pin is connected to.
     * This value is optional, but higly recommended!
     */
    int8_t  readyPin;


public:
    /**
     * Default constructor.
     * @param enablePin the GPIO of MCU that is connected to ENABLE of TCA4307 buffer.
     * @param readyPin  the GPIO of MCU that is connected to READY of TCA4307 buffer, defaults to -1 (no pin).
     */
    explicit Ulm_TCA4307(uint8_t enablePin, int8_t readyPin = -1);


    /**
     * Default destructor.
     */
    ~Ulm_TCA4307() override;


    /**
     * Sets up the required pins and wakes up the i2c buffer. If a ready pin
     * is set, then the status of the i2c buffer will be checked.
     * @return {@code true} if device is running, {@code false} if there is
     * an error and the buffer does not start. Please note: If no ready pin is
     * given, then no errors can be detected and this method will return {@code true}.
     */
    bool begin() override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char * getDeviceName() const override;


    /**
     * Put buffer into sleep-mode. This will decouple the I2C-bus from the MCU and
     * save energy.
     * @return {@code true}, if device entered sleep mode within the given timeout, {@code false} otherwise.
     * Please note: If ready pin is not set, then this method will always return {@code true}.
     */
    [[nodiscard]] bool sleep(uint16_t timeoutMillis = 3000) const;


    /**
     * Wakes up buffer. The buffer must be awake before using i2c bus.
     * @return {@code true} if device is awake within the given timeout, {@code false} otherwise.
     * Please note: If ready pin is not set, then this method will always return {@code true}.
     */
    [[nodiscard]] bool wake(uint16_t timeoutMillis = 3000) const;


private:
    /**
     * Checks, if the ready pin of the buffer reacts correctly within a given timespan. If the ready pin is not set,
     * then this method will immediately return {@code true}.
     * @param expectedStatus the expected status (for power-down: LOW, for active: HIGH)
     * @param timeoutMillis the timeout in milliseconds
     * @return {@code true}, if the pin has the expected status within the given timeout, {@code false} otherwise.
     */
    [[nodiscard]] bool checkReadyPinStatusWithinTimeout(uint8_t expectedStatus, uint16_t timeoutMillis) const;


    /**
     * Checks, if this buffer has a valid ready pin set or not.
     * @return {@code true}, if a valid ready pin was passed to constructor, {@code false} otherwise.
     */
    [[nodiscard]] bool hasReadyPin() const;
};


#endif //ULM_WEATHERBALLOON_ULM_TCA4307_H
