//
// Created by Falko Alrik Schmidt on 15.05.24.
//

#include "Ulm_I2C_Accessor.h"

Ulm_I2C_Accessor::Ulm_I2C_Accessor(uint8_t address, TwoWire &i2c) : address(address), i2c(i2c) {
    // Nothing to do here.
}


bool Ulm_I2C_Accessor::startInterface() {
    // Only begin interface, if the begin() method was not called already.
    if(INITIALISED_I2C_INTERFACES.find(&(this->i2c)) == INITIALISED_I2C_INTERFACES.end()) {
        // Not yet in init list -> call begin and then push it into list.
        this->i2c.begin();
        INITIALISED_I2C_INTERFACES.insert(&(this->i2c));
    }

    // Check, if device is available and ACKs.
    this->i2c.beginTransmission(this->address);
    return this->i2c.endTransmission(true) == 0;
}


bool Ulm_I2C_Accessor::write(const uint8_t *buf, size_t len, bool stopBit) const {
    if(buf == nullptr) {
        return false;
    }
    this->i2c.beginTransmission(this->address);
    const auto bytesWritten = this->i2c.write(buf, len);
    const auto endTransmission = this->i2c.endTransmission(stopBit);
    return (bytesWritten == len) && (endTransmission == 0);
}


bool Ulm_I2C_Accessor::write8(uint8_t val, bool stopBit) const {
    return this->write(&val, 1, stopBit);
}


bool Ulm_I2C_Accessor::read(uint8_t *buf, size_t len, bool stopBit) const {
    const auto receivedBytes = this->i2c.requestFrom(
            this->address,
            static_cast<uint8_t>(len),
            stopBit
    );

    if(receivedBytes == len) {
        // Only fill buffer, if the amount of received bytes matches the length.
        return this->i2c.readBytes(buf, len) == len;
    }

    // Otherwise we still need to read the amount of received bytes, but they
    // will not be saved anywhere.
    for(auto i = 0; i < receivedBytes; i++) {
        this->i2c.read();
    }
    return false;
}


bool
Ulm_I2C_Accessor::writeThenRead(const uint8_t *sendBuf, size_t sendLen, uint8_t *recvBuf, size_t recvLen, bool stopBit) const {
    if(!this->write(sendBuf, sendLen, stopBit)) {
        return false;
    }
    return this->read(recvBuf, recvLen, true);
}


bool Ulm_I2C_Accessor::read8(uint8_t *val, bool stopBit) const {
    return this->read(val, 1, stopBit);
}


bool Ulm_I2C_Accessor::read16(uint16_t *val, bool stopBit) const {
    uint8_t buf[2] = {};
    if(!this->read(buf, 2, stopBit)) {
        return false;
    }
    *val = buf[0] << 8 | buf[1];
    return true;
}


bool Ulm_I2C_Accessor::read24(uint32_t *val, bool stopBit) const {
    uint8_t buf[3];
    if(!this->read(buf, 3, stopBit)) {
        return false;
    }
    *val = static_cast<uint32_t>(buf[0]) << 16 | static_cast<uint32_t>(buf[1]) << 8 | static_cast<uint32_t>(buf[2]);
    *val &= 0x00FFFFFF;
    return true;
}


bool Ulm_I2C_Accessor::read32(uint32_t *val, bool stopBit) const {
    uint8_t buf[4];
    if(!this->read(buf, 4, stopBit)) {
        return false;
    }
    *val = static_cast<uint32_t>(buf[0]) << 24 | static_cast<uint32_t>(buf[1]) << 16 | static_cast<uint32_t>(buf[2]) << 8 | static_cast<uint32_t>(buf[3]);
    return true;
}
