// ===========================================================================
// This file is created due to the Arduino library guidelines.
// Including this file will include all relevant header files from this
// library.
//
// Falko Schmidt, 2023
// ===========================================================================
#ifndef ULM_WEATHERBALLOON_ULM_WEATHERBALLOON_H
#define ULM_WEATHERBALLOON_ULM_WEATHERBALLOON_H

// --------------------------------------------
// ACTORS
// --------------------------------------------
// LEDs
#include "actors/Ulm_LED.h"
#include "actors/Ulm_LED_BuiltIn.h"
#include "actors/Ulm_NeoPixel.h"

// Mechanical devices
#include "actors/Ulm_Button.h"
#include "actors/Ulm_IlluminatedButton.h"

// Displays
#include "actors/Ulm_OLED_Display.h"
#include "actors/Ulm_TFT.h"

// Printer
#include "actors/Ulm_ThermalPrinter.h"


// --------------------------------------------
// DEBUGGING
// --------------------------------------------
#include "debugging/Ulm_SerialDebugger.h"


// --------------------------------------------
// INTERFACES
// --------------------------------------------
#include "interfaces/Ulm_I2C_Accessor.h"
#include "interfaces/Ulm_TCA4307.h"


// --------------------------------------------
// SENSORS
// --------------------------------------------
// Acceleration
#include "sensors/accelerometers/Ulm_ADXL343.h"

// Battery
#include "sensors/battery/Ulm_MAX17048.h"
#include "sensors/battery/Ulm_MCP73833.h"

// Gas
#include "sensors/gas/Ulm_ENS16X.h"
#include "sensors/gas/Ulm_SCD4X.h"

// Magnetic
#include "sensors/magnetometers/Ulm_LIS3MDL.h"

// Memory
#include "sensors/memory/Ulm_RP2040_MemoryUnit.h"

// Pressure altimeters
#include "sensors/pressure_altimeter/Ulm_PressureAltimeterSensor.h"
#include "sensors/pressure_altimeter/Ulm_BMP280.h"
#include "sensors/pressure_altimeter/Ulm_LPS22H.h"
#include "sensors/pressure_altimeter/Ulm_MS5611.h"

// RTC
#include "sensors/rtc/Ulm_RTC.h"
#include "sensors/rtc/Ulm_DS3231.h"
#include "sensors/rtc/Ulm_PCF8523.h"

// Temperature & Humidity sensors
#include "sensors/thermohygrometer/Ulm_Thermohygrometer.h"
#include "sensors/thermohygrometer/Ulm_SHT4X.h"

// Temperature sensors
#include "sensors/thermometer/Ulm_Thermometer.h"
#include "sensors/thermometer/Ulm_DS18B20.h"
#include "sensors/thermometer/Ulm_MCP9700.h"

// Voltage sensors
#include "sensors/voltmeter/Ulm_VoltageDivider.h"


// --------------------------------------------
// STORAGE
// --------------------------------------------
#include "storage/Ulm_SDStorage.h"


// --------------------------------------------
// UTILS
// --------------------------------------------
#include "util/PinUtil.h"
#include "util/Ulm_Testable.h"


#endif //ULM_WEATHERBALLOON_ULM_WEATHERBALLOON_H
