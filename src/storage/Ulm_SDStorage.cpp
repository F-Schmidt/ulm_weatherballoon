//
// Created by Falko Alrik Schmidt on 09.09.23.
//

#include "Ulm_SDStorage.h"

Ulm_SDStorage::Ulm_SDStorage() : SdFat32(), Ulm_Device() {
    this->csPin                     = SS;       // Default SPI SS pin of Arduino.
    this->detectPin                 = -1;       // No detect pin.
    this->directory                 = "/";      // Root directory on SD card.
    this->dataFileName              = String(); // No data file.
    this->redundancyFileName        = String(); // No redundancy file.
    this->extremeEnvironmentLogic   = false;    // No additional safety measurements.
}


void Ulm_SDStorage::setCsPin(const uint8_t csPin) {
    this->csPin = csPin;
}


void Ulm_SDStorage::setDetectPin(const int8_t detectPin) {
    this->detectPin = detectPin;
}


void Ulm_SDStorage::setExtremeEnvironmentLogic(bool extremeLogic) {
    this->extremeEnvironmentLogic = extremeLogic;
}


bool Ulm_SDStorage::begin() {
    // Check, if SD card is inserted.
    if (this->detectPin >= 0) {
        pinMode(this->detectPin, INPUT);
        if (digitalRead(this->detectPin) == HIGH) {
            // No SD card inserted!
            return false;
        }
    }

    // Init the SD, open it etc.
    const bool sdInit = SdFat32::begin(this->csPin); //SDClass::begin(this->csPin);

    // Check, if the given directory is the root.
    if(strcmp(this->directory.c_str(), "/") != 0) { //TODO check if this is necessary or if exists is enough!
        // Check, if the given directory already exists.
        if(!exists(this->directory)) {
            // If not, we will create it. If the creation fails, then
            // we will end the connection to SD and report a fail.
            if (!mkdir(this->directory)) {
                end();
                return false;
            }
        }
    }

    // Check data file name for errors
    if (this->dataFileName.length() == 0) {
        // Not set -> error!
        end();
        return false;
    }

    // Create data file
    const bool dfInit = this->determineCurrentLogFileName(this->directory, this->dataFileName); //TODO check limitations of file name
    if(!this->hasRedundancyFile()) {
        // If no redundancy file, then we are done and can check for extreme conditions set.
        if(this->extremeEnvironmentLogic) {
            end();
        }
        return sdInit && dfInit;
    }

    // Create redundancy file
    const bool rfInit = this->determineCurrentLogFileName(this->directory, this->redundancyFileName);
    if(this->extremeEnvironmentLogic) {
        end();
    }
    return sdInit && dfInit && rfInit;
}


bool Ulm_SDStorage::determineCurrentLogFileName(const String &dir, String &fileName) {
    // Determine the correct file name for data file
    if (fileName.length() == 0) {
        // Not set -> error!
        return false;
    }

    const int numberIndex = fileName.indexOf('0');
    while (SdFat32::exists(dir + "/" + fileName)) {
        if (fileName[numberIndex + 2] != '9') {
            fileName[numberIndex + 2]++;
        } else if (fileName[numberIndex + 1] != '9') {
            fileName[numberIndex + 1]++;
            fileName[numberIndex + 2] = '0';
        } else if (fileName[numberIndex] != '9') {
            fileName[numberIndex]++;
            fileName[numberIndex + 1] = '0';
            fileName[numberIndex + 2] = '0';
        } else {
            // All files from 0 to 999 exist -> error!
            return false;
        }
    }
    if (File32 f = SdFat32::open(dir + "/" + fileName, FILE_WRITE)) {
        f.close();
        return true;
    }
    return false;
}


bool Ulm_SDStorage::store(const char *str) {

    const size_t dataLength = strlen(str);
    const size_t dfWritten = storeToFile(str, this->directory, this->dataFileName);

    if (this->hasRedundancyFile()) {
        const size_t redWritten = storeToFile(str, this->directory, this->redundancyFileName);
        return dfWritten == dataLength && redWritten == dataLength;
    }
    return dfWritten == dataLength;
}


size_t Ulm_SDStorage::storeToFile(const char *str, const String &dir, const String &fileName) {
    String canonicalFileName;
    if(dir.equals("/")) {
        canonicalFileName = "/" + fileName;
    } else {
        canonicalFileName = dir + "/" + fileName;
    }

    size_t bytesWritten = 0;
    if (File32 f = SdFat32::open(canonicalFileName, FILE_WRITE)) {
        bytesWritten = f.write(str);
        f.flush();
        f.close();
    }
    return bytesWritten;
}


void Ulm_SDStorage::setDirectory(const String &directory) {
    Ulm_SDStorage::directory = directory;
}


void Ulm_SDStorage::setDataFileName(const String &dataFileName) {
    Ulm_SDStorage::dataFileName = dataFileName;
}


void Ulm_SDStorage::setRedundancyFileName(const String &redundancyFileName) {
    Ulm_SDStorage::redundancyFileName = redundancyFileName;
}


Ulm_SDStorage::Ulm_SDStorage_Builder::Ulm_SDStorage_Builder() {
    this->sdStorage = Ulm_SDStorage();
}


// ====================================================================================================================
// ====================================================================================================================
// ====================================================================================================================


Ulm_SDStorage::Ulm_SDStorage_DetectPinBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::atPin(uint8_t pin) {
    this->sdStorage.setCsPin(pin);
    return *this;
}

Ulm_SDStorage::Ulm_SDStorage_DirectoryBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::withDetectPin(int8_t detectPin) {
    this->sdStorage.setDetectPin(detectPin);
    return *this;
}

Ulm_SDStorage::Ulm_SDStorage_DirectoryBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::withoutDetectPin() {
    this->sdStorage.setDetectPin(-1);
    return *this;
}

Ulm_SDStorage::Ulm_SDStorage_DataFileBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::atDirectory(const String &dir) {
    if (const unsigned int dirNameLength = dir.length(); dirNameLength >= 1 && dirNameLength <= 8) { //TODO check limitations of file name
        // Supported directory name length
        this->sdStorage.setDirectory(dir);
    }
    return *this;
}

Ulm_SDStorage::Ulm_SDStorage_DataFileBuilder &Ulm_SDStorage::Ulm_SDStorage_Builder::atRootDirectory() {
    this->sdStorage.setDirectory("/");
    return *this;
}


String Ulm_SDStorage::determineGenericLogFileName(const String &prefix, const FileType fileType) {
    String fileName = prefix + "000.";
    switch (fileType) {
        case CSV:   fileName += "csv";  break;
        case LOG:   fileName += "log";  break;
        default:    fileName += "txt";  break;
    }
    return fileName;
}


bool Ulm_SDStorage::hasRedundancyFile() const {
    return this->redundancyFileName.length() > 0;
}


Ulm_SDStorage::Ulm_SDStorage_CSPinBuilder& Ulm_SDStorage::builder() {
    static std::vector<Ulm_SDStorage_Builder> builder;
    builder.emplace_back();
    return builder.back();
}


const char *Ulm_SDStorage::getDeviceName() const {
    return "SD-Card Storage"; //TODO add redundant if it has redundancy file.
}


Ulm_SDStorage::Ulm_Storage_OptionalArgsBuilder &
Ulm_SDStorage::Ulm_SDStorage_Builder::withDataFile(const String &dataFilePrefix,
                                                   Ulm_SDStorage::FileType fileType) {
    this->sdStorage.setDataFileName(Ulm_SDStorage::determineGenericLogFileName(dataFilePrefix, fileType));
    return *this;
}


Ulm_SDStorage::Ulm_Storage_OptionalArgsBuilder &
Ulm_SDStorage::Ulm_SDStorage_Builder::withRedundancyFile(const String &redFilePrefix,
                                                         Ulm_SDStorage::FileType fileType) {
    this->sdStorage.setRedundancyFileName(Ulm_SDStorage::determineGenericLogFileName(redFilePrefix, fileType));
    return *this;
}


Ulm_SDStorage::Ulm_Storage_OptionalArgsBuilder &
Ulm_SDStorage::Ulm_SDStorage_Builder::withLogicForExtremeEnvironments() {
    this->sdStorage.setExtremeEnvironmentLogic(true);
    return *this;
}

Ulm_SDStorage Ulm_SDStorage::Ulm_SDStorage_Builder::build() {
    return this->sdStorage;
}
