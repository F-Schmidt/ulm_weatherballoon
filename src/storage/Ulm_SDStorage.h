//
// Created by Falko Alrik Schmidt on 09.09.23.
//

#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H

#include <Arduino.h>
#include "SdFat.h"
#include "Ulm_Device.h"

/**
 * This class represents and SD storage with optional detection pin.
 * Some special additions, like data redundancy and other additional actions
 * for extreme environments are implemented to be used in high altitude balloons.
 *
 * Please note, that rather than directly instantiating this class, you want to use
 * the according builder.
 *
 * @see Ulm_SDStorage::builder
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage final : SdFat32, public Ulm_Device {

public:
    /**
     * This enum represents valid and supported file types.
     */
    enum FileType {
        CSV,
        LOG,
        TXT
    };

    class Ulm_SDStorage_Builder;
    class Ulm_SDStorage_CSPinBuilder;
    class Ulm_SDStorage_DetectPinBuilder;
    class Ulm_SDStorage_DirectoryBuilder;
    class Ulm_SDStorage_DataFileBuilder;
    class Ulm_Storage_OptionalArgsBuilder;

private:
    /**
     * The physical pin, that will be used to select the
     * SD card via SPI bus.
     */
    uint8_t csPin;

    /**
     * The physical pin, that is connected to the detection
     * pin of the SD card slot.
     */
    int8_t detectPin;

    /**
     * The directory in which all log files are created.
     */
    String directory;

    /**
     * The data file name (primary file).
     */
    String dataFileName;

    /**
     * The redundancy file name (if any).
     */
    String redundancyFileName;

    /**
     * Whether additional security measurements for
     * extreme environments should be taken or not.
     */
    bool extremeEnvironmentLogic;

    /**
     * Default constructor.
     */
    Ulm_SDStorage();


public:
    /**
     * This static method can be used to receive a builder object for storage creation.
     * @return a new builder.
     * @see Ulm_SDStorage_Builder
     */
    static Ulm_SDStorage_CSPinBuilder & builder();

    /**
     * Initialises the SD card, creates the directory and all needed files.
     * After calling this method, you can directly start storing data into files.
     * @return {@code true} if, and only if, everything went as planned
     * and without ANY error, {@code false} otherwise.
     */
    bool begin() override;

    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /*template <typename... Args>
    bool storef(const char* format, Args&& args) {
        return false; //TODO implement. forward to file printf
        //         this->logf(DEBUG, DEBUG_PREFIX, format, std::forward<Args>(args)...);
    }*/


    // TODO NEW: This will be added in future.
    // bool printf(const char *format, ...) __attribute__ ((format (printf, 2, 3)));


    /**
     * Stores a const char* in data and optional redundancy file.
     * @param str the string of type const char* to be stored.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool store(const char* str);

    /**
     * Sets the chip-select pin of the SD card.
     * @param csPin the physical pin, which the SD card is connected to.
     */
    void setCsPin(uint8_t csPin);

    /**
     * Sets the detect pin of the SD card slot. If no detect pin is available or unused,
     * then set the parameter to -1.
     * @param detectPin the physical pin, which the SD card slot is connected to.
     */
    void setDetectPin(int8_t detectPin);

    /**
     * Sets the directory, in which all log files are stored. Please note, that subdirectories
     * are not supported. You can either use the root directory ("/") or DIRECT subdirs
     * within the root.
     * @param directory
     */
    void setDirectory(const String &directory);

    /**
     * Sets the file name of primary data file.
     * @param dataFileName the data file name.
     */
    void setDataFileName(const String &dataFileName);

    /**
     * Sets the file name of the optional redundancy file.
     * @param redundancyFileName the redundancy file name.
     */
    void setRedundancyFileName(const String &redundancyFileName);

    /**
     * Sets whether additional actions for extreme environments
     * should be taken or not. Please note, that using this feature
     * will result in overhead in flash and execution time.
     * @param extremeEnvironmentLogic whether additional actions
     * should be taken or not.
     */
    void setExtremeEnvironmentLogic(bool extremeEnvironmentLogic);


private:
    /**
     * Determines the generic log file name.
     * Example: prefix: DATA_, filetype: csv
     * Then the result of this method will be 'DATA_000.csv'.
     * @param prefix the prefix of the file, if any.
     * @param fileType the file type of this file.
     * @return the overall log file name from given values.
     * The number will always be set to '000' by this method.
     */
    static String determineGenericLogFileName(const String& prefix, Ulm_SDStorage::FileType fileType);

    /**
     * Determines a usable file using the generic log file name.
     * Example: dir: DATALOG, fileName: DATA_000.csv
     * Then this method will search the SD cards for the files
     *  DATALOG/DATA_000.csv
     *  DATALOG/DATA_001.csv
     *  ...
     *  DATALOG/DATA_999.csv
     *  and return after the first file name, that does not yet exist on the SD card.
     * @param dir the directory, in which the file is stored.
     * @param fileName the file name after calling {@see #determineGenericLogFileName}.
     * @return {@code true} if a valid file name was found, {@code false} if all files
     * from 000 to 999 already exist on SD card.
     */
    bool determineCurrentLogFileName(const String& dir, String& fileName);

    /**
     * Stores string into a file.
     * @param str the string to be stored.
     * @param dir the directory name, in which the data file is located.
     * @param fileName the name of the file, that will be written to.
     * @return the number of bytes written into the file.
     */
    size_t storeToFile(const char *str, const String& dir, const String& fileName);

    /**
     * Determines, if this SD storage has a redundancy file or not.
     * @return {@code true} if this storage has a redundancy file, {@code false} otherwise.
     */
    [[nodiscard]] bool hasRedundancyFile() const;
};


// ------------------------------------------------------------------------------------------------------------
// FIRST STEP - MANDATORY - SET CS PIN OF SD CARD
// ------------------------------------------------------------------------------------------------------------
/**
 * This class represents the first mandatory step of building an SD Storage, which is
 * setting the chip-select pin of the SD card.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_SDStorage_CSPinBuilder {
public:

    /**
     * Default destructor.
     */
    virtual ~Ulm_SDStorage_CSPinBuilder() = default;

    /**
     * Configures the physical pin, which the chip-select pin of the SD card is connected to.
     * @param pin the physical pin of CS of SD card.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_SDStorage_DetectPinBuilder& atPin(uint8_t pin) = 0;
};


/**
 * This class represents the second mandatory step of building an SD Storage, which is
 * setting a detection pin. To also support other hardware-structures, it is possible
 * to not set any detection pin.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_SDStorage_DetectPinBuilder {
public:

    /**
     * Default destructor.
     */
    virtual ~Ulm_SDStorage_DetectPinBuilder() = default;

    /**
     * Configures the physical pin, which the detection pin of the SD card is connected to.
     * The detection pin will tell you, if an SD card is inserted or not.
     * @param detectPin the physical pin of the detection pin of your SD card slot.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_SDStorage_DirectoryBuilder& withDetectPin(int8_t detectPin) = 0;

    /**
     * Call this method, if your hardware does not support any detection pin.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_SDStorage_DirectoryBuilder& withoutDetectPin() = 0;
};


/**
 * This class represents the third mandatory step of building an SD Storage, which is
 * configuring a directory, in which all files will be created.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_SDStorage_DirectoryBuilder {
public:

    /**
     * Default destructor.
     */
    virtual ~Ulm_SDStorage_DirectoryBuilder() = default;

    /**
     * Configures the directory, in which the data and redundancy files will be stored.
     * This allows you to have a somehow ordered structure on your SD card.
     * @param dir the directory name.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_SDStorage_DataFileBuilder& atDirectory(const String& dir) = 0;

    /**
     * If you do not want to create any additional directory on your SD card, but instead
     * just directly save all files in your top-level directory, then call this method.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_SDStorage_DataFileBuilder& atRootDirectory() = 0;
};


/**
 * This class represents the fourth mandatory step of building an SD Storage, which is
 * configuring the data file name.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_SDStorage_DataFileBuilder {
public:

    /**
     * Default destructor.
     */
    virtual ~Ulm_SDStorage_DataFileBuilder() = default;

    /**
     * Configures the data file name using given prefix and file type.
     * @param dataFilePrefix the prefix of the data file.
     * @param fileType the type of the file, that will be created.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_Storage_OptionalArgsBuilder& withDataFile(const String& dataFilePrefix, enum FileType fileType) = 0;

    /**
     * Just a small method for convenience, defaulting to CSV file type.
     * @param dataFilePrefix the prefix of the data file.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_Storage_OptionalArgsBuilder& withDataFile(const String& dataFilePrefix) {
        return this->withDataFile(dataFilePrefix, CSV);
    }
};


/**
 * This class represents the last step(s) of building an SD storage.
 * All of these features can be used optionally - there is no need to!
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_Storage_OptionalArgsBuilder {
public:
    /**
     * Default destructor.
     */
    virtual ~Ulm_Storage_OptionalArgsBuilder() = default;

    /**
     * Configures optional data redundancy by creating a redundancy file, which will
     * contain the same data as the prior defined data file. This feature is very useful
     * to prevent eventual data corruption, but it comes at the cost of higher flash usage
     * and runtime. The time needed to store the data doubles.
     * @param redFilePrefix the prefix of the redundancy file.
     * @param fileType the type of the file, that will be created.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_Storage_OptionalArgsBuilder& withRedundancyFile(const String& redFilePrefix, FileType fileType) = 0;

    /**
     * Convenience method for CSV file type.
     * @param redFilePrefix the prefix of the redundancy file.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_Storage_OptionalArgsBuilder& withRedundancyFile(const String& redFilePrefix) {
        return this->withRedundancyFile(redFilePrefix, CSV);
    }

    /**
     * If you use this class for some extreme environments (cold, hot, high radiation,...),
     * then you can simply call this method to add some additional actions to hopefully
     * safely store your data.
     * @return a pointer to this builder to enable method chaining.
     */
    virtual Ulm_Storage_OptionalArgsBuilder&
    withLogicForExtremeEnvironments() = 0;

    /**
     * Finally, we want to create our SD Storage! Once you are happy with your configuration, you
     * can call this method, which will return the SD Storage configured the way to wanted to.
     * @return the SD Storage according to your arguments.
     */
    virtual Ulm_SDStorage build() = 0;
};


/**
 * This class is the actual builder class for Ulm_SDStorage. It is implemented in a step-builder
 * pattern, meaning, that a certain order of method calls is enforced. This enables better
 * bug finding and ensures, that the SD Storage runs with valid and all required values.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_SDStorage::Ulm_SDStorage_Builder final : public Ulm_SDStorage_CSPinBuilder,
                                                   public Ulm_SDStorage_DetectPinBuilder,
                                                   public Ulm_SDStorage_DirectoryBuilder,
                                                   public Ulm_SDStorage_DataFileBuilder,
                                                   public Ulm_Storage_OptionalArgsBuilder {

    /**
     * The actual Ulm_SDStorage. Normally, the builder class has the same arguments, that
     * the final object also has. This works great, if you have enough resources, like on
     * a normal computer. Here we are working on a microcontroller with VERY limited amount
     * of RAM and flash storage. Therefore, the builder is simply a wrapper requiring no
     * additional storage space compared to the actual SD storage itself.
     */
    Ulm_SDStorage sdStorage;

public:
    /**
     * Default constructor.
     */
    Ulm_SDStorage_Builder();

    /**
     * Configures the physical pin, which the chip-select pin of the SD card is connected to.
     * @param pin the physical pin of CS of SD card.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_SDStorage_DetectPinBuilder &atPin(uint8_t pin) override;

    /**
     * Configures the physical pin, which the detection pin of the SD card is connected to.
     * The detection pin will tell you, if an SD card is inserted or not.
     * @param detectPin the physical pin of the detection pin of your SD card slot.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_SDStorage_DirectoryBuilder &withDetectPin(int8_t detectPin) override;

    /**
     * Call this method, if your hardware does not support any detection pin.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_SDStorage_DirectoryBuilder &withoutDetectPin() override;

    /**
     * The directory, in which all logs will be created.
     * Please note: The maximum length of the directory name is
     * restricted to 8 chars! Also, the directory will only consist
     * of capital letters, no matter what your input to this method is.
     * Also do not end this string with a '/'. This will be done automatically
     * internally! If the given directory name is invalid, then the root directory
     * will be used instead.
     * @param dir the directory name.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_SDStorage_DataFileBuilder &atDirectory(const String& dir) override;

    /**
     * If you do not want to create any additional directory on your SD card, but instead
     * just directly save all files in your top-level directory, then call this method.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_SDStorage_DataFileBuilder &atRootDirectory() override;

    /**
     * Configures the data file name using given prefix and file type.
     * If you for example pass these values: prefix: DATA_ and fileType: CSV, then the resulting name will
     * be DATA_000.csv where '000' will automatically be counted up on every start (existing files are not
     * appended to or overwritten).
     * @param dataFilePrefix the prefix of the data file.
     * @param fileType the type of the file, that will be created.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_Storage_OptionalArgsBuilder &withDataFile(const String& dataFilePrefix, FileType fileType) override;

    /**
     * Configures optional data redundancy by creating a redundancy file, which will
     * contain the same data as the prior defined data file. This feature is very useful
     * to prevent eventual data corruption, but it comes at the cost of higher flash usage
     * and runtime. The time needed to store the data doubles.
     * @param redFilePrefix the prefix of the redundancy file.
     * @param fileType the type of the file, that will be created.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_Storage_OptionalArgsBuilder &withRedundancyFile(const String& redFilePrefix, FileType fileType) override;

    /**
     * If you use this class for some extreme environments (cold, hot, high radiation,...),
     * then you can simply call this method to add some additional actions to hopefully
     * safely store your data.
     * @return a pointer to this builder to enable method chaining.
     */
    Ulm_Storage_OptionalArgsBuilder &withLogicForExtremeEnvironments() override;

    /**
     * Finally, we want to create our SD Storage! Once you are happy with your configuration, you
     * can call this method, which will return the SD Storage configured the way to wanted to.
     * @return the SD Storage according to your arguments.
     */
    Ulm_SDStorage build() override;
};

#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_SDSTORAGE_H
