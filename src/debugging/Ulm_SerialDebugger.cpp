/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a more
 * 'logging-style' solution for the serial interface.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_SerialDebugger.h"
#include "actors/Ulm_LED_BuiltIn.h"


Ulm_SerialDebugger::Ulm_SerialDebugger(HardwareSerial &debugging_stream, const Ulm_SerialDebuggerLogLevel logLevel)
    : debuggingStream(debugging_stream), logLevel(logLevel) {
    this->begun = false;
}


bool Ulm_SerialDebugger::begin(const unsigned long baudrate) {
    this->debuggingStream.begin(baudrate, SERIAL_8N1);
    this->begun = true;
    delay(3000); // This was found to be the most reliable way to start serial interface in RP2040.
    return true;
}


bool Ulm_SerialDebugger::begin() {
    return this->begin(115200);
}


const char *Ulm_SerialDebugger::getDeviceName() const {
    return "Serial Debugger";
}


void Ulm_SerialDebugger::setLogLevel(const Ulm_SerialDebuggerLogLevel logLevel) {
    this->logLevel = logLevel;
}


void Ulm_SerialDebugger::end() {
    this->debuggingStream.end();
    this->begun = false;
}


void Ulm_SerialDebugger::log(const Ulm_SerialDebuggerLogLevel msgLevel, const char *prefix, const char *msg) const {
    // Check, if stream is still active.
    if (!this->begun) {
        return;
    }

    // Check, if message should be logged according to log level.
    if (msgLevel < this->logLevel) {
        return;
    }

    // Write prefix.
    this->debuggingStream.print('[');
    this->debuggingStream.print(prefix);
    this->debuggingStream.print("] ");

    // Write message.
    this->debuggingStream.println(msg);
}



void Ulm_SerialDebugger::debug(const char *msg) const {
    this->log(DEBUG, DEBUG_PREFIX, msg);
}


void Ulm_SerialDebugger::info(const char *msg) const {
    this->log(INFO, INFO_PREFIX, msg);
}


void Ulm_SerialDebugger::warn(const char *msg) const {
    this->log(WARN, WARN_PREFIX, msg);
}


void Ulm_SerialDebugger::error(const char *msg) const {
    this->log(ERROR, ERROR_PREFIX, msg);
}
