/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a more
 * 'logging-style' solution for the serial interface.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_SERIALDEBUGGERIMPL_H
#define ULM_WEATHERBALLOON_SERIALDEBUGGERIMPL_H

#include <Arduino.h>
#include "Ulm_Device.h"


/**
 * This class provides a lightweight and easy-to-use solution
 * for logging messages to the serial interface.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
class Ulm_SerialDebugger final : public Ulm_Device {


public:
    /**
     * All possible logging levels.
     */
    enum Ulm_SerialDebuggerLogLevel : uint8_t {
        DEBUG   = 0,
        INFO    = 1,
        WARN    = 2,
        ERROR   = 3
    };


private:
    /**
     * The output to send the messages to.
     */
    HardwareSerial &debuggingStream;

    
    /**
     * The minimum required log level that a message must have
     * to be output to serial.
     */
    Ulm_SerialDebuggerLogLevel logLevel;

    
    /**
     * This value traces the current state of the underlying
     * stream. If it was closed, calling {@code end()}, then
     * no further message will be output.
     */
    bool begun;


public:
    /**
     * Default constructor.
     * @param debugging_stream The debugging output stream, defaults to Serial.
     * @param logLevel the required message log level for output, defaults to DEBUG outputting all messages.
     */
    explicit Ulm_SerialDebugger(HardwareSerial &debugging_stream = Serial, Ulm_SerialDebuggerLogLevel logLevel = DEBUG);

    
    /**
     * Starts the debugging interface with a baud rate of 115200.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;

    
    /**
     * Starts the debugging interface with the given baudrate.
     * @param baudrate the transfer speed for the debugging interface.
     * @return {@code true} on success, {@code false} on error.
     */
    [[nodiscard]] bool begin(unsigned long baudrate);

    
    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /**
     * Updates the log level for message output. This method has immediate
     * effect and can be changed whenever you would like it.
     * @param logLevel the new log level, that a message must have to be
     *                 output to the debugging interface.
     */
    void setLogLevel(Ulm_SerialDebuggerLogLevel logLevel);


    /**
     * Ends the debugging stream and no further messages will be output.
     */
    void end();


    /**
     * Outputs a debugging message.
     * @param msg the message to be output.
     */
    void debug(const char* msg) const;


    /**
     * Outputs a formatted debugging message.
     * @tparam Args The types of the arguments used in the formatted message.
     * @param format The format string specifying how to format the output message.
     *               It should follow standard printf-style formatting rules.
     * @param args The arguments that will be formatted and inserted into the output string.
     */
    template <typename... Args>
    void debugf(const char *format, Args&&... args) const {
        this->logf(DEBUG, DEBUG_PREFIX, format, std::forward<Args>(args)...);
    }


    /**
     * Outputs an information message.
     * @param msg the information message.
     */
    void info(const char* msg) const;


    /**
     * Outputs a formatted information message.
     * @tparam Args The types of the arguments used in the formatted message.
     * @param format The format string specifying how to format the output message.
     *               It should follow standard printf-style formatting rules.
     * @param args The arguments that will be formatted and inserted into the output string.
     */
    template <typename... Args>
    void infof(const char *format, Args&&... args) const {
        this->logf(INFO, INFO_PREFIX, format, std::forward<Args>(args)...);
    }


    /**
     * Outputs a warning message.
     * @param msg the warning message.
     */
    void warn(const char* msg) const;


    /**
     * Outputs a formatted warning message.
     * @tparam Args The types of the arguments used in the formatted message.
     * @param format The format string specifying how to format the output message.
     *               It should follow standard printf-style formatting rules.
     * @param args The arguments that will be formatted and inserted into the output string.
     */
    template <typename... Args>
    void warnf(const char *format, Args&&... args) const {
        this->logf(WARN, WARN_PREFIX, format, std::forward<Args>(args)...);
    }


    /**
     * Outputs an error message.
     * @param msg the error message.
     */
    void error(const char* msg) const;

    
    /**
     * Outputs a formatted error message.
     * @tparam Args The types of the arguments used in the formatted message.
     * @param format The format string specifying how to format the output message.
     *               It should follow standard printf-style formatting rules.
     * @param args The arguments that will be formatted and inserted into the output string.
     */
    template <typename... Args>
    void errorf(const char *format, Args&&... args) const {
        this->logf(ERROR, ERROR_PREFIX, format, std::forward<Args>(args)...);
    }


private:
    /**
     * Writes a message to the debugging interface according to the given parameters.
     * For class-internal use only.
     * @param msgLevel  the log level of the message. If it is at greater or equal to the
     *                  log level of the debugger, then the message will be output.
     * @param prefix    a message prefix, that indicates the severity of the message itself.
     * @param msg       the actual message.
     */
    void log(Ulm_SerialDebuggerLogLevel msgLevel, const char* prefix, const char* msg) const;


    /**
     * Writes a formatted message to the debugging interface according to the given parameters.
     * For class-internal use only.
     * @tparam Args The types of the arguments used in the formatted message.
     * @param msgLevel the log level of the message. If it is at greater or equal to the
     *                 log level of the debugger, then the message will be output.
     * @param prefix a message prefix, that indicates the severity of the message itself.
     * @param format The format string specifying how to format the output message.
     *               It should follow standard printf-style formatting rules.
     * @param args The arguments that will be formatted and inserted into the output string.
     */
    template <typename... Args>
    void logf(const Ulm_SerialDebuggerLogLevel msgLevel, const char *prefix, const char *format,
                                  Args&&... args) const {
        // Check, if stream is still active.
        if (!this->begun) {
            return;
        }

        // Check, if message should be logged according to log level.
        if (msgLevel < this->logLevel) {
            return;
        }

        // Write prefix.
        this->debuggingStream.print('[');
        this->debuggingStream.print(prefix);
        this->debuggingStream.print("] ");

        // Write formatted message and add new line for convenience.
        this->debuggingStream.printf(format, std::forward<Args>(args)...);
        this->debuggingStream.println();
    }


    /* Message prefixes */
    static inline const auto DEBUG_PREFIX  = "PROG";
    static inline const auto INFO_PREFIX   = "INFO";
    static inline const auto WARN_PREFIX   = "WARN";
    static inline const auto ERROR_PREFIX  = "FAIL";
};


#endif //ULM_WEATHERBALLOON_SERIALDEBUGGERIMPL_H
