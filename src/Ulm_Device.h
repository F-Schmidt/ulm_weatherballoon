/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H
#define UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H

/**
 * All electrical components need some sort of initialisation process
 * after the microcontroller receives power. Sadly, the arduino framework
 * does not provide a common name for an init-method and so there are multiple
 * wild names in libraries, like {@code begin()}, {@code init()}, {@code start()}
 * or others.
 *
 * To at least provide a uniform name within this library, all components will
 * inherit this class and therefore must implement the virtual method {@code begin(...)}.
 *
 * @author Falko Schmidt
 * @since 0.1.0
 */
class Ulm_Device {

public:
    /**
     * Virtual destructor.
     */
    virtual ~Ulm_Device() = default;


    /**
     * Initializes the component.
     * @return {@code true} on, and only on, success without ANY error, {@code false} otherwise.
     */
    virtual bool begin() = 0;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] virtual const char* getDeviceName() const = 0;
};


#endif //UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_BEGINNABLE_H
