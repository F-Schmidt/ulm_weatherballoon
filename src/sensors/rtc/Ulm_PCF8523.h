/*
* This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out RTC of
 * type PCF8523.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_PCF8523_H
#define ULM_WEATHERBALLOON_ULM_PCF8523_H

#include "RTClib.h"
#include "interfaces/Ulm_I2C_Accessor.h"
#include "Ulm_RTC.h"

/* Definition for sensor-specific sample */
typedef Ulm_RTC_Sample Ulm_PCF8523_Sample;

/**
 * This class can access RTCs of type PCF8523.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
class Ulm_PCF8523 final : RTC_PCF8523, public Ulm_RTC<Ulm_PCF8523_Sample>, public virtual Ulm_I2C_Accessor {

public:
    /**
     * Default constructor.
     * @param address the i2c address of this chip, defaults to 0x68.
     * @param i2c the i2c bus, that this sensor is connected to.
     * @param northernHemisphere whether we are in northern or southern hemisphere (required for seasons), defaults
     * to {@code true}, indicating northern hemisphere.
     */
    explicit Ulm_PCF8523(uint8_t address = 0x68, TwoWire &i2c = Wire, bool northernHemisphere = true);


    /**
     * Inits the RTC.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;


    /**
     * Reads out the real-time clock module.
     * @param sample the sample to store all data in.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool sample(Ulm_PCF8523_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;

    void adjustTime(const DateTime& dt) override;
    ::DateTime readTime() override;
};


#endif //ULM_WEATHERBALLOON_ULM_PCF8523_H
