/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out RTC of
 * type DS3231.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_DS3231.h"

Ulm_DS3231::Ulm_DS3231(const uint8_t address, TwoWire &i2C, const bool northernHemisphere)
        : Ulm_RTC(northernHemisphere), Ulm_I2C_Accessor(address, i2C) {
    // Nothing more to do here.
}


bool Ulm_DS3231::sample(Ulm_DS3231_Sample &sample) {
    sample.time = this->now();
    sample.lostPower = this->lostPower();
    sample.temperature = this->getTemperature();
    return Ulm_RTC::sample(sample);
}


bool Ulm_DS3231::begin() {
    if (!RTC_DS3231::begin(&this->i2c)) {
        return false;
    }

    if (this->lostPower()) {
        // Time is lost, so we will at least set it to the script compile time.
        this->adjust(DateTime(F(__DATE__), F(__TIME__)));
    }

    // No clock output at pin.
    this->disable32K();

    return true;
}


const char *Ulm_DS3231::getDeviceName() const {
    return "DS3231";
}

void Ulm_DS3231::adjustTime(const DateTime &dt) {
    adjust(dt);
}

DateTime Ulm_DS3231::readTime() {
    return this->now();
}
