/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out RTC of
 * type DS3231.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_DS3231_H
#define ULM_WEATHERBALLOON_ULM_DS3231_H

#include "Ulm_RTC.h"
#include "RTClib.h"
#include "interfaces/Ulm_I2C_Accessor.h"

struct Ulm_DS3231_Sample : Ulm_RTC_Sample {
    float temperature{};
};


class Ulm_DS3231 final : RTC_DS3231, public Ulm_RTC<Ulm_DS3231_Sample>, public Ulm_I2C_Accessor {

public:
    explicit Ulm_DS3231(uint8_t address = 0x68, TwoWire &i2C = Wire, bool northernHemisphere = true);
    bool begin() override;
    bool sample(Ulm_DS3231_Sample &sample) override;
    [[nodiscard]] const char *getDeviceName() const override;
    void adjustTime(const DateTime& dt) override;
    DateTime readTime() override;
};


#endif //ULM_WEATHERBALLOON_ULM_DS3231_H
