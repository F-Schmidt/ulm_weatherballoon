/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a generic
 * structure for real-time clock modules.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_RTC_H
#define ULM_RTC_H

#include <ctime>
#include <regex>
#include <RTClib.h>
#include "sensors/Ulm_Sensor.h"


/**
 * Enum representing all possible seasons.
 */
enum Ulm_RTC_Season : uint8_t {
    WINTER = 0,
    SPRING = 1,
    SUMMER = 2,
    FALL   = 3
};


/**
 * Data sample for real time clock.
 */
struct Ulm_RTC_Sample {
    DateTime time;
    bool lostPower;
    Ulm_RTC_Season season;
};


/**
 * This class provides a unique interface for real-time clock modules.
 * @tparam T The generic type of data sample to use.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
template<typename T = Ulm_RTC_Sample>
class Ulm_RTC : public Ulm_Sensor<T> {

    /* Compile-time check for sample type matching. */
    static_assert(
        std::is_base_of_v<Ulm_RTC_Sample, T>,
        "RTC implementation must use a descendant of Ulm_RTC_Sample as generic type!"
    );


private:
    /**
     * Whether usage is on northern or southern hemisphere.
     */
    bool northernHemisphere;


public:
    /**
     * Default constructor.
     * @param northern_hemisphere whether the RTC is used in northern hemisphere or not. This only has effect
     * on season calculation.
     */
    explicit Ulm_RTC(const bool northern_hemisphere = true) : northernHemisphere(northern_hemisphere) {}


    /**
     * Fills up all fields depending on the values in the sample.
     * @param sample the sensor data sample to fill up.
     * @return always {@code true}.
     */
    bool sample(T &sample) override {
        sample.season = this->calcSeason(sample.time);
        return true;
    }


    virtual void adjustTime(const DateTime &dt) = 0;
    virtual DateTime readTime() = 0;

    /**
     * Sets the stored time in RTC to the given one.
     * @param dateTimeStr the time to set the RTC to in valid ISO8601 format.
     * @return {@code true} on success, {@code false} if an error occur.
     */
    bool setTime(const char* dateTimeStr) {
        // Regex pattern for "YYYY-MM-DD HH:MM:SS".
        static const std::regex pattern(R"(^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$)");

        // Object to hold the matched arguments.
        std::smatch match;

        // Try to match the datetime string against the regex pattern.
        if (const auto s = std::string(dateTimeStr); !std::regex_match(s, match, pattern)) {
            // Invalid format -> return.
            return false;
        }

        // Extract all components (year, month, day, hour, minute, second) from string.
        const int year    = std::stoi(match[1].str());
        const int month   = std::stoi(match[2].str());
        const int day     = std::stoi(match[3].str());
        const int hour    = std::stoi(match[4].str());
        const int minute  = std::stoi(match[5].str());
        const int second  = std::stoi(match[6].str());
        auto newTime = DateTime(year, month, day, hour, minute, second);

        // Store current time in RTC...
        this->adjustTime(newTime);

        // ... and check, if it is within 1 second.
        const auto currentTime = this->readTime();
        return currentTime >= newTime && currentTime <= newTime + TimeSpan(1);
    }


private:
    /**
     * Calculates the current meteorological season resulting in either spring, summer, fall or winter.
     * @param time the time to calculate the season for.
     * @return a season represented as enum value.
     *
     * Source for calculation: https://en.wikipedia.org/wiki/Season
     */
    Ulm_RTC_Season calcSeason(const DateTime &time) {
        switch (time.month()) { // <- ranges from 1 - 12.
            case 3:
            case 4:
            case 5:  return this->northernHemisphere ? SPRING : FALL;
            case 6:
            case 7:
            case 8:  return this->northernHemisphere ? SUMMER : WINTER;
            case 9:
            case 10:
            case 11: return this->northernHemisphere ? FALL   : SPRING;
            default: return this->northernHemisphere ? WINTER : SUMMER;
        }
    }
};

#endif //ULM_RTC_H
