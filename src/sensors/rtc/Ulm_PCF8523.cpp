/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out RTC of
 * type PCF8523.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_PCF8523.h"

Ulm_PCF8523::Ulm_PCF8523(const uint8_t address, TwoWire &i2c, const bool northernHemisphere)
        : Ulm_I2C_Accessor(address, i2c), Ulm_RTC(northernHemisphere) {
    // Nothing more to do here
}


bool Ulm_PCF8523::begin() {
    if(!startInterface()) {
        return false;
    }

    if(!RTC_PCF8523::begin()) {
        return false;
    }

    if(!this->initialized() || this->lostPower()) {
        // No time set in RTC -> use the compilation time.
        RTC_PCF8523::adjust(DateTime(F(__DATE__), F(__TIME__)));
    }

    // Disable all timers, they are not needed in the weatherballoon.
    this->disableCountdownTimer();
    this->disableSecondTimer();

    // When the RTC was stopped and stays connected to the battery, it has
    // to be restarted by clearing the STOP bit. Let's do this to ensure
    // the RTC is running.
    this->start();

    // RTC is running, yay!
    return true;
}


bool Ulm_PCF8523::sample(Ulm_PCF8523_Sample &sample) {
    sample.time = this->now();
    sample.lostPower = this->lostPower();
    return Ulm_RTC::sample(sample);
}


const char *Ulm_PCF8523::getDeviceName() const {
    return "PCF8523";
}

void Ulm_PCF8523::adjustTime(const DateTime &dt) {
    this->adjust(dt);
}

DateTime Ulm_PCF8523::readTime() {
    return this->now();
}
