/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a generic
 * structure for altitude sensors.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_GENERICALTITUDESENSOR_H
#define ULM_WEATHERBALLOON_ULM_GENERICALTITUDESENSOR_H


#include "sensors/thermometer/Ulm_Thermometer.h"


/**
 * Generic sample for altitude sensors.
 */
struct Ulm_PressureAltimeterSensor_Sample : Ulm_Thermometer_Sample {
    float pressure{};
    float altitude{};
    float waterBoilingPoint{};
};


/**
 * Generic altitude sensor class. More information for this sensor type:
 * https://en.wikipedia.org/wiki/Pressure_altimeter
 *
 * @tparam T the generic type of the data samples.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
template<typename T = Ulm_PressureAltimeterSensor_Sample>
class Ulm_PressureAltimeterSensor : public Ulm_Thermometer<T> {

    /* Compile-time check for sample type matching. */
    static_assert(
        std::is_base_of_v<Ulm_PressureAltimeterSensor_Sample, T>,
        "Altitude sensor implementation must use a descendant of Ulm_AltitudeSensor_Sample as generic type!"
    );


public:
    /**
     * Calculates all values from sample content.
     * @param sample the sample from which data will be used to calculate values
     *               and calculated values will be stored in.
     * @return always true.
     */
    bool sample(T &sample) override {
        sample.altitude = calcAltitude(sample.pressure);
        sample.waterBoilingPoint = calcWaterBoilingPoint(sample.pressure);
        return true;
    }


private:
    /**
     * Calculates the water boiling point for a given pressure.
     * @param pressure the pressure to calculate the boiling point at.
     * @return the water boiling point in degree Celsius.
     *
     * Source: Currently copied from Adafruit library.
     */
    static float calcWaterBoilingPoint(const float pressure) {
        if (pressure == 0.0F) {
            return NAN;
        }
        // Magnus formula for calculation of the boiling point of water at a given pressure.
        return 234.175f * std::log(pressure / 6.1078f) / (17.08085f - std::log(pressure / 6.1078f));
    }


    /**
     * Calculates the altitude from given pressure.
     * @param pressure the pressure to calculate altitude from.
     * @return the calculated altitude in meters.
     *
     * Source for calculation: https://en.wikipedia.org/wiki/Pressure_altitude
     */
    static float calcAltitude(const float pressure) {
        return 476924.063818F * (1 - std::pow(pressure / 1013.25F, 0.190284F));
    }
};


#endif //ULM_WEATHERBALLOON_ULM_GENERICALTITUDESENSOR_H
