/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type LPS22H.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_LPS22H.h"


Ulm_LPS22H::Ulm_LPS22H(const uint8_t address, TwoWire &wire) : Adafruit_LPS22(), Ulm_I2C_Accessor(address, wire) {
    //nothing to do here.
}


bool Ulm_LPS22H::begin() {
    if(!begin_I2C(this->address, &this->i2c)) {
        return false;
    }
    // Only sample once and then go back to sleep.
    this->setDataRate(LPS22_RATE_ONE_SHOT);
    return true;
}


bool Ulm_LPS22H::sample(Ulm_LPS22H_Sample &sample) {
    sensors_event_t temp, pressure;
    if(!this->getEvent(&pressure, &temp)) {
        return false;
    }

    sample.temperature = temp.temperature;
    sample.pressure = pressure.pressure;
    return Ulm_PressureAltimeterSensor::sample(sample);
}


const char *Ulm_LPS22H::getDeviceName() const {
    return "LPS22H";
}
