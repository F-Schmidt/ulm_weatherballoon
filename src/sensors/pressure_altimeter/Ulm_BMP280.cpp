/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type BMP280.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_BMP280.h"


Ulm_BMP280::Ulm_BMP280(const uint8_t address, TwoWire &wire)
        : Adafruit_BMP280(&Wire), Ulm_I2C_Accessor(address, wire) {
    //Nothing to do here.
}


bool Ulm_BMP280::begin() {
    if(!Adafruit_BMP280::begin(this->address)) {
        return false;
    }

    this->setSampling(
            MODE_FORCED,
            SAMPLING_X2,
            SAMPLING_X16,
            FILTER_X16,
            STANDBY_MS_500
    );
    return true;
}


bool Ulm_BMP280::sample(Ulm_BMP280_Sample &sample) {
    if(!this->takeForcedMeasurement()) {
        return false;
    }

    // First, read temperature and pressure. (Units: Celsius and hPa)
    const auto readTemperature = this->readTemperature();
    const auto readPressure = this->readPressure() / 100.0f;

    // Make sure that temperature is within sensor spec (-40...85C)
    if(readTemperature < -40.0f || readTemperature > 85.0f) {
        return false;
    }

    // Check, if pressure is within sensor spec (300...1100 hPa)
    if(readPressure < 300.0f || readPressure > 1100.0f) {
        return false;
    }

    // Update all values in struct.
    sample.temperature = readTemperature;
    sample.pressure = readPressure;
    return Ulm_PressureAltimeterSensor::sample(sample);
}


const char *Ulm_BMP280::getDeviceName() const {
    return "BMP280";
}