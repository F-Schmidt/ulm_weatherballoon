/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type MS5611.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_PressureAltimeterSensor.h"
#include "Ulm_MS5611.h"


constexpr uint8_t ULM_MS5611_COMMAND_RESET        = 0x1E;
constexpr uint8_t ULM_MS5611_COMMAND_CONVERT_D1   = 0x40;
constexpr uint8_t ULM_MS5611_COMMAND_CONVERT_D2   = 0x50;
constexpr uint8_t ULM_MS5611_COMMAND_ADC_READ     = 0x00;
constexpr uint8_t ULM_MS5611_COMMAND_PROM_READ    = 0xA0;


Ulm_MS5611::Ulm_MS5611(const uint8_t address, TwoWire &i2c)
        : Ulm_PressureAltimeterSensor(), Ulm_I2C_Accessor(address, i2c) {
    // Nothing to do here.
}


bool Ulm_MS5611::begin() {
    // Check, if the given I2C address is valid.
    if(this->address != 0x76 && this->address != 0x77) {
        return false;
    }

    // Start i2c and check if device is connected.
    if(!startInterface()) {
        return false;
    }

    // The device must be resettet once after power-up
    // to correctly load ROM values.
    if(!this->softReset()) {
        return false;
    }

    // Read out the calibration constants from sensor ROM.
    // CRC will be performed to ensure data integrity.
    if(!this->readCalibrationConstantsFromROM()) {
        return false;
    }

    // Configure sensor to use highest OSR for best readings.
    this->setOversampling(OSR_4096);

    // That is all, the sensor is set!
    return true;
}


bool Ulm_MS5611::sample(Ulm_MS5611_Sample &t) {
    // Read digital pressure and temperature data.
    uint32_t d1, d2;
    if(!this->readDigitalFromSensor(ULM_MS5611_COMMAND_CONVERT_D1, d1)) {
        return false;
    }

    if(!this->readDigitalFromSensor(ULM_MS5611_COMMAND_CONVERT_D2, d2)) {
        return false;
    }

    const double dT   = d2 - C[5] * pow(2, 8);
    double temp = (2000 + dT * C[6] / pow(2, 23)) / 100;

    // Calculate temperature compensated pressure.
    double off  = C[2] * pow(2, 17) + C[4] * dT / pow(2, 6);
    double sens = C[1] * pow(2, 16) + C[3] * dT / pow(2, 7);

    // Variables needed for second order temperature compensation.
    double t2    = 0;
    double off2  = 0;
    double sens2 = 0;

    // Second order temperature compensation.
    if(temp < 20.0) {
        // Low temperature
        t2    = dT * dT / pow(2, 31);
        off2  = 5 * pow(temp - 2000, 2) / 2;
        sens2 = 5 * pow(temp - 2000, 2) / 4;

        if(temp < -15.0) {
            // Very low temperature
            off2  = off2 + 7 * pow(temp + 1500, 2);
            sens2 = sens2 + 11 * pow(temp + 1500, 2) / 2;
        }
    }

    // Calculate pressure and temperature with compensation.
    temp = temp - t2;
    off  = off - off2;
    sens = sens - sens2;
    const double p = (d1 * sens / pow(2, 21) - off) / pow(2, 15) / 100;

    t.temperature = static_cast<float>(temp);
    t.pressure = static_cast<float>(p);
    return Ulm_PressureAltimeterSensor::sample(t);
}


const char *Ulm_MS5611::getDeviceName() const {
    return "MS561101BA03-50";
}


bool Ulm_MS5611::softReset() const {
    // Reset device
    if(!this->write8(ULM_MS5611_COMMAND_RESET)) {
        return false;
    }

    // According to datasheet page 10, the sensor requires 2.8ms
    // after reset. We will wait 3ms.
    delay(3);

    // Success and waiting time passed -> return.
    return true;
}


void Ulm_MS5611::setOversampling(const Ulm_MS5611_Oversampling oversampling) {
    this->osr = oversampling;
}


// This code is just very slightly adapted. The original CRC-calculation can be found in AN250:
// https://www.amsys-sensor.com/downloads/notes/MS5XXX-C-code-example-for-MS56xx-MS57xx-MS58xx-AMSYS-an520e.pdf
uint8_t Ulm_MS5611::calculateCRC4() {
    uint16_t n_rem = 0; // remainder of the division
    const uint16_t crc_read = C[7]; // save the read CRC
    C[7] = 0xFF00 & C[7]; // CRC byte is replaced with zero

    for (uint8_t cnt = 0; cnt < 16; cnt++) { // operation is performed on bytes
        if (cnt & 0b1u) {
            n_rem ^= static_cast<uint8_t>(C[cnt >> 1] & 0x00FF);
        } else {
            n_rem ^= static_cast<uint8_t>(C[cnt >> 1] >> 8);
        }

        for (uint8_t n_bit = 8; n_bit > 0; n_bit--) {
            if (n_rem & 0x8000) {
                n_rem = n_rem << 1 ^ 0x3000;
            } else {
                n_rem = n_rem << 1;
            }
        }
    }

    n_rem = 0x000F & n_rem >> 12; // final 4-bit remainder is CRC code
    this->C[7] = crc_read;        // restore the CRC byte

    return n_rem ^ 0x00;
}


bool Ulm_MS5611::readDigitalFromSensor(const uint8_t cmd, uint32_t& result) const {
    if(!this->write8(cmd + this->osr)) {
        return false;
    }

    // Wait according to conversion time
    unsigned int waitMicros;
    switch (this->osr) {
        case OSR_256:   waitMicros = 900;   break;  // max. conversion time:  600 us
        case OSR_512:   waitMicros = 3000;  break;  // max. conversion time: 1170 us
        case OSR_1024:  waitMicros = 4000;  break;  // max. conversion time: 2280 us
        case OSR_2048:  waitMicros = 6000;  break;  // max. conversion time: 4540 us
        default:        waitMicros = 10000; break;  // max. conversion time: 9040 us
    }
    delayMicroseconds(waitMicros);

    // Fetch values from i2c.
    if(!this->write8(ULM_MS5611_COMMAND_ADC_READ)) {
        return false;
    }

    uint32_t val;
    if(!this->read24(&val)) {
        return false;
    }
    result = val;
    return true;
}


bool Ulm_MS5611::readCalibrationConstantsFromROM() {
    for(uint8_t reg = 0; reg < 8; reg++) {
        if(!this->write8(ULM_MS5611_COMMAND_PROM_READ | (reg & 0b111) << 1)) {
            // Interface / transmission error -> return!
            return false;
        }
        uint16_t readValue;
        if(!this->read16(&readValue)) {
            return false;
        }
        this->C[reg] = readValue;
    }
    return this->calculateCRC4() != (this->C[7] & 0xFu);
}
