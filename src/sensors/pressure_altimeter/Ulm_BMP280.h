/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type BMP280.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_BMP280_H
#define ULM_WEATHERBALLOON_ULM_BMP280_H


#include <Adafruit_BMP280.h>
#include "Ulm_Weatherballoon.h"


typedef Ulm_PressureAltimeterSensor_Sample Ulm_BMP280_Sample;


class Ulm_BMP280 final : Adafruit_BMP280, public Ulm_PressureAltimeterSensor<Ulm_BMP280_Sample>, Ulm_I2C_Accessor {


public:
    /**
     * Default constructor.
     * @param address the sensor address.
     * @param wire the i2c connection, that the sensor uses.
     */
    explicit Ulm_BMP280(uint8_t address = 0x76, TwoWire& wire = Wire);


    /**
     * Inits the sensor.
     * @return true on success, false on error.
     */
    bool begin() override;


    /**
     * Reads out all values from the sensor.
     * @param sample the sample to store all data into. 
     * @return true on success, false on error.
     */
    bool sample(Ulm_BMP280_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;

};


#endif //ULM_WEATHERBALLOON_ULM_BMP280_H
