/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type MS5611.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_MS5611_H
#define ULM_WEATHERBALLOON_ULM_MS5611_H


#include "Ulm_PressureAltimeterSensor.h"
#include "interfaces/Ulm_I2C_Accessor.h"


/**
 * The different oversampling-rates of MS5611 sensor.
 */
enum Ulm_MS5611_Oversampling : uint8_t {
    OSR_256     = 0,    // <- Default case.
    OSR_512     = 2,
    OSR_1024    = 4,
    OSR_2048    = 6,
    OSR_4096    = 8
};


/* struct definition for consistent naming */
typedef Ulm_PressureAltimeterSensor_Sample Ulm_MS5611_Sample;


/**
 * This class represents sensors of type MS5611.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_MS5611 final : public Ulm_PressureAltimeterSensor<Ulm_MS5611_Sample>, public Ulm_I2C_Accessor {


private:
    /**
     * Calibration constants stored in ROM of sensor.
     * These values will be called 'C' and stored in an
     * array to match the datasheet formulas.
     */
    uint16_t C[8] = {0};

    /**
     * Oversampling rate of the sensor. The higher it is, the more accurate the
     * readings will be, but it will require more time and energy.
     */
    Ulm_MS5611_Oversampling osr = OSR_256;


public:
    /**
     * Default constructor.
     * @param address the address of this sensor.
     * @param i2c the i2c connection that this sensor uses.
     */
    explicit Ulm_MS5611(uint8_t address = 0x77, TwoWire &i2c = Wire);


    /**
     * Inits the sensor
     * @return true on success, false on error.
     */
    bool begin() override;


    /**
     * Reads out all values from the sensor.
     * @param t the sample to store all data into.
     * @return true on success, false on error.
     */
    bool sample(Ulm_MS5611_Sample &t) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /**
     * Updates the oversampling rate.
     * @param oversampling the new oversampling rate
     */
    void setOversampling(Ulm_MS5611_Oversampling oversampling);


private:
    /**
     * Executes a soft reset of the sensor.
     * @return true on success, false on error.
     */
    bool softReset() const;


    /**
     * Reads calibration variables from sensor that will later be used
     * for temperature and pressure measurement.
     * @return true on success, false on error.
     */
    bool readCalibrationConstantsFromROM();


    /**
     * Checksum calculation for this sensor specifically.
     * @return the calculated crc4 checksum.
     */
    uint8_t calculateCRC4();


    /**
     * Reads raw values from sensors that will then be calibrated to
     * get the measured temperature and pressure.
     * @param cmd the command, that the sensor shall execute.
     * @param result the read-out result / response from sensor.
     * @return true on success, false on error.
     */
    bool readDigitalFromSensor(uint8_t cmd, uint32_t& result) const;
};


#endif //ULM_WEATHERBALLOON_ULM_MS5611_H
