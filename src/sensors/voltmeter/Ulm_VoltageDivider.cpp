/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out the voltage
 * over a voltage divider.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_VoltageDivider.h"

Ulm_VoltageDivider::Ulm_VoltageDivider(const uint8_t analogPin, const float lowerResistor, const float upperResistor,
                                       const uint8_t adcBits, const float adcVoltage) {
    this->analogPin     = analogPin;
    this->upperResistor = upperResistor;
    this->lowerResistor = lowerResistor;
    this->adcBits       = adcBits;
    this->adcVoltage    = adcVoltage;
}


const char *Ulm_VoltageDivider::getDeviceName() const {
    return "Voltage Divider";
}


bool Ulm_VoltageDivider::begin() {
    if(!PinUtil::isAnalogPin(this->analogPin)) {
        return false;
    }
    pinMode(this->analogPin, INPUT);
    analogReadResolution(this->adcBits);
    return true;
}


bool Ulm_VoltageDivider::sample(Ulm_VoltageDivider_Sample &sample) {
    // First, read the voltage at the analog pin...
    const auto maxADCValue = static_cast<float>(1 << this->adcBits);
    const auto readADCValue = static_cast<float>(analogRead(this->analogPin));
    const auto rawVoltage = (readADCValue / maxADCValue) * this->adcVoltage;

    // ...and then calculate the real voltage. The upper resistor is connected between
    // the voltage to be measured and the analog pin, the lower resistor is connected
    // between analog pin and GND.
    const auto divisionFactor = (this->upperResistor + this->lowerResistor) / this->lowerResistor;
    const auto calculatedVoltage = rawVoltage * divisionFactor;

    sample.rawVoltage = rawVoltage;
    sample.voltage    = calculatedVoltage;
    return true;
}
