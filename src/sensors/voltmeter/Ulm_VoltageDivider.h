/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out the voltage
 * over a voltage divider.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_VOLTAGEDIVIDER_H
#define ULM_WEATHERBALLOON_ULM_VOLTAGEDIVIDER_H


#include <Arduino.h>
#include "Ulm_Weatherballoon.h"


struct Ulm_VoltageDivider_Sample {
    float rawVoltage;
    float voltage;
};


/**
 * This class reads out a simple voltage divider and can
 * therefore be used to check battery voltage or similar.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_VoltageDivider final : public Ulm_Sensor<Ulm_VoltageDivider_Sample> {


private:
    /**
     * The analog pin, that the voltage divider is connected to.
     */
    uint8_t analogPin;


    /**
     * The absolute value in ohms of the resistor between
     * analog MCU pin and GND.
     */
    float lowerResistor;


    /**
     * The absolute value in ohms of the resistor between
     * voltage to be measured and analog MCU pin.
     */
    float upperResistor;


    /**
     * The ADC resolution of the MCU.
     */
    uint8_t adcBits;


    /**
     * The ADC reference voltage of the MCU.
     */
    float adcVoltage;


public:
    /**
     * Default constructor.
     * @param analogPin the pin, that the voltage divider is connected to. Should be analog!
     * @param lowerResistor the lower resistor from analog pin to GND.
     * @param upperResistor the upper resistor from analog pin to measured voltage.
     * @param adcBits the ADC read resolution in bits, defaults to 10.
     * @param adcVoltage the ADC reference voltage, defaults to 5.0V.
     */
    explicit Ulm_VoltageDivider(uint8_t analogPin, float lowerResistor, float upperResistor,
                                uint8_t adcBits = 12, float adcVoltage = 3.3f);


    /**
     * Initializes the component.
     * @return {@code true} on success without ANY error, {@code false} if a non-analog pin is used.
     */
    bool begin() override;


    /**
     * Reads out the voltage.
     * @param sample the sample to store all values into.
     * @return always {@code true}. No errors can be detected.
     */
    bool sample(Ulm_VoltageDivider_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name.
     */
    [[nodiscard]] const char *getDeviceName() const override;
};


#endif //ULM_WEATHERBALLOON_ULM_VOLTAGEDIVIDER_H
