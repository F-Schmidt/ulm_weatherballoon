/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides access to
 * memory information.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_RP2040_MemoryUnit.h"
#include "Arduino.h"


Ulm_RP2040_MemoryUnit::Ulm_RP2040_MemoryUnit() = default;


bool Ulm_RP2040_MemoryUnit::begin() {
    // No need to start anything.
    return true;
}


bool Ulm_RP2040_MemoryUnit::sample(Ulm_RP2040_MemoryUnit_Sample &sample) {
    sample.freeHeap     = rp2040.getFreeHeap();
    sample.usedHeap     = rp2040.getUsedHeap();
    sample.totalHeap    = rp2040.getTotalHeap();
    sample.stackPointer = rp2040.getStackPointer();
    sample.freeStack    = rp2040.getFreeStack();
    return true;
}


const char * Ulm_RP2040_MemoryUnit::getDeviceName() const {
    return "RP2040 memory unit";
}
