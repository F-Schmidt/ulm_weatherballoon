/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides access to
 * memory information.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_RP2040_MEMORYUNIT_H
#define ULM_RP2040_MEMORYUNIT_H


#include <cstdint>
#include <sensors/Ulm_Sensor.h>


/**
 * Struct to hold all information regarding memory.
 */
struct Ulm_RP2040_MemoryUnit_Sample {
    int freeHeap;
    int usedHeap;
    int totalHeap;
    uint32_t stackPointer;
    uint32_t freeStack;
};


/**
 * This class allows gathering memory information of an RP2040
 * to for example detect memory leaks.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
class Ulm_RP2040_MemoryUnit final : public Ulm_Sensor<Ulm_RP2040_MemoryUnit_Sample> {


public:
    /**
     * Default constructor.
     */
    Ulm_RP2040_MemoryUnit();


    /**
     * This method does nothing, because nothing needs to be
     * started to get memory information. Just kept due to
     * generic interface of Ulm_Device.
     * @return always true.
     */
    bool begin() override;


    /**
     * Reads out memory information.
     * @param sample data sample to store information into.
     * @return always true.
     */
    bool sample(Ulm_RP2040_MemoryUnit_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char * getDeviceName() const override;
};



#endif //ULM_RP2040_MEMORYUNIT_H
