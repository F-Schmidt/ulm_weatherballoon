/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_MCP73833.h"

Ulm_MCP73833::Ulm_MCP73833(const int8_t pinStat1, const int8_t pinStat2, const int8_t pinPg)
        : pinStat1(pinStat1), pinStat2(pinStat2), pinPG(pinPg) {
    this->currentStatus = UNKNOWN;
}


void ulm_mcp73833_interrupt_forwarder(void* ptr) {
    static_cast<Ulm_MCP73833*>(ptr)->updateStatus();
}


bool Ulm_MCP73833::begin() {
    pinMode(this->pinStat1, INPUT_PULLUP);
    pinMode(this->pinStat2, INPUT_PULLUP);
    pinMode(this->pinPG,    INPUT_PULLUP);

    attachInterruptParam(this->pinStat1, ulm_mcp73833_interrupt_forwarder, CHANGE, this);
    attachInterruptParam(this->pinStat2, ulm_mcp73833_interrupt_forwarder, CHANGE, this);
    attachInterruptParam(this->pinPG,    ulm_mcp73833_interrupt_forwarder, CHANGE, this);

    this->updateStatus();
    return true;
}


bool Ulm_MCP73833::sample(Ulm_MCP73833_Sample &t) {
    // Make sure, that status is updated.
    this->updateStatus();

    // Write values into sample.
    t.status = this->currentStatus;
    t.error  = (this->currentStatus == TEMPERATURE_FAULT || this->currentStatus == UNKNOWN);
    return true;
}


void Ulm_MCP73833::updateStatus() {
    // Read out all status pins.
    uint8_t read = 0;
    read |= (digitalRead(this->pinPG)    == HIGH ? 1 : 0) << 0;
    read |= (digitalRead(this->pinStat2) == HIGH ? 1 : 0) << 1;
    read |= (digitalRead(this->pinStat1) == HIGH ? 1 : 0) << 2;

    // Set the new status regarding the current status.
    switch (read) {
        case 0b111u: this->currentStatus = SHUTDOWN; break;
        case 0b110u:
            // Either STANDBY or TEMPERATURE FAULT. This depends on the previous status.
            switch (this->currentStatus) {
                case SHUTDOWN:
                case SYSTEM_TEST_MODE:
                    this->currentStatus = STANDBY;
                    break;
                default:
                    this->currentStatus = TEMPERATURE_FAULT;
                    break;
            };
            break;
        case 0b010u: this->currentStatus = CHARGING;         break;
        case 0b100u: this->currentStatus = CHARGE_COMPLETE;  break;
        case 0b000u: this->currentStatus = SYSTEM_TEST_MODE; break;
        default:     this->currentStatus = UNKNOWN;          break;
    }
}


const char * Ulm_MCP73833::getDeviceName() const {
    return "MCP73833";
}
