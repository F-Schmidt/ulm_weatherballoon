/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_MAX17048.h"


Ulm_MAX17048::Ulm_MAX17048(TwoWire &i2C, const int8_t alertPin) : i2cHandle(i2C), alertPin(alertPin) {
    // Nothing more to do here.
}


bool Ulm_MAX17048::hasAlertPin() const {
    return this->alertPin >= 0;
}


void ulm_max17048_interrupt_forwarder(void* ptr) {
    static_cast<Ulm_MAX17048 *>(ptr)->readAlerts();
}


void Ulm_MAX17048::readAlerts() {
    if (!this->isActiveAlert()) {
        return;
    }

    const uint8_t statusFlags = this->getAlertStatus();
    if(statusFlags & MAX1704X_ALERTFLAG_SOC_CHANGE) {
        // STATE OF CHARGE CHANGE BY AT LEAST 1%
        this->clearAlertFlag(MAX1704X_ALERTFLAG_SOC_CHANGE);
    }
    if(statusFlags & MAX1704X_ALERTFLAG_SOC_LOW) {
        // ALERT IS SET, IF BATTERY IS EMPTY
        this->clearAlertFlag(MAX1704X_ALERTFLAG_SOC_LOW);
    }
    if (statusFlags & MAX1704X_ALERTFLAG_VOLTAGE_RESET) {
        // ALERT IS SET AFTER THE DEVICE HAS BEEN RESET IF EnVr IS SET
        this->clearAlertFlag(MAX1704X_ALERTFLAG_VOLTAGE_RESET);
    }
    if (statusFlags & MAX1704X_ALERTFLAG_VOLTAGE_LOW) {
        // ALERT IS SET, IF THE VOLTAGE IS LESS THAN THE LOW VOLTAGE THRESHOLD
        this->clearAlertFlag(MAX1704X_ALERTFLAG_VOLTAGE_LOW);
    }
    if (statusFlags & MAX1704X_ALERTFLAG_VOLTAGE_HIGH) {
        // ALERT IS SET, IF THE VOLTAGE IS GREATER THAN THE HIGH VOLTAGE THRESHOLD
        this->clearAlertFlag(MAX1704X_ALERTFLAG_VOLTAGE_HIGH);
    }
    if (statusFlags & MAX1704X_ALERTFLAG_RESET_INDICATOR) {
        // THIS IS SET ON POWERUP AND IF THE DEVICE IS NOT YET CONFIGURED
        this->clearAlertFlag(MAX1704X_ALERTFLAG_RESET_INDICATOR);
    }
}


bool Ulm_MAX17048::begin() {
    if(!Adafruit_MAX17048::begin(&this->i2cHandle)) {
        return false;
    }

    // Prevent hibernation and set voltage alerts
    // according to one-cell battery voltage levels.
    this->setHibernationThreshold(0.0);
    this->setAlertVoltages(2.5, 4.2);


    // Configure alert pin, if available.
    if(this->hasAlertPin()) {
        pinMode(this->alertPin, INPUT_PULLUP);
        attachInterruptParam(this->alertPin, ulm_max17048_interrupt_forwarder, FALLING, this);
    }

    return this->isDeviceReady();
}


bool Ulm_MAX17048::sample(Ulm_MAX17048_Sample &sample) {
    // Wake up device, if it is sleeping to read out
    // the latest values.
    if(this->isHibernating()) {
        this->wake();
    }

    // Read cell voltage. If it is invalid, then there is
    // an error with the sensor or interface.
    const float readCellVoltage = this->cellVoltage();
    if(isnan(readCellVoltage)) {
        return false;
    }

    // In this case, the sensor and interface are valid
    // and the values in the given sample can be updated.
    sample.cellVoltage = readCellVoltage;
    sample.cellPercent = this->cellPercent();
    sample.chargeRate  = this->chargeRate();

    if(!this->hasAlertPin()) {
        // In this case, there is no alert pin. This results in no alerts being read out as soon as they occur,
        // so we manually need to check them. This will be done here. Only relevant alerts will be checked.
        bool success = true;
        if(const uint8_t alert = this->getAlertStatus(); alert & MAX1704X_ALERTFLAG_VOLTAGE_LOW) {
            sample.cellVoltageOk = false;
            success &= this->clearAlertFlag(MAX1704X_ALERTFLAG_VOLTAGE_LOW);
        } else if(alert & MAX1704X_ALERTFLAG_VOLTAGE_HIGH) {
            sample.cellVoltageOk = false;
            success &= this->clearAlertFlag(MAX1704X_ALERTFLAG_VOLTAGE_HIGH);
        } else {
            sample.cellVoltageOk = true;
        }
        return success;
    }

    return true;
}


const char * Ulm_MAX17048::getDeviceName() const {
    return "MAX17048";
}
