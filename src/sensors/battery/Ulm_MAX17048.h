/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_MAX17048_H
#define ULM_WEATHERBALLOON_ULM_MAX17048_H


#include "sensors/Ulm_Sensor.h"
#include "Adafruit_MAX1704X.h"


struct Ulm_MAX17048_Sample {
    float cellVoltage;
    float cellPercent;
    float chargeRate;
    bool cellVoltageOk;
};


/**
 * This class controls MAX17048 li-ion battery monitor sensors.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
class Ulm_MAX17048 final : public Ulm_Sensor<Ulm_MAX17048_Sample>, Adafruit_MAX17048 {


private:
    TwoWire& i2cHandle;
    int8_t alertPin;


public:
    explicit Ulm_MAX17048(TwoWire &i2C = Wire, int8_t alertPin = -1);
    bool begin() override;
    bool sample(Ulm_MAX17048_Sample &t) override;
    [[nodiscard]] const char * getDeviceName() const override;

    friend void ulm_max17048_interrupt_forwarder(void* ptr);


private:
    [[nodiscard]] bool hasAlertPin() const;
    void readAlerts();
};


#endif //ULM_WEATHERBALLOON_ULM_MAX17048_H
