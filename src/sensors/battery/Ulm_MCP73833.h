/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_MCP73833_H
#define ULM_WEATHERBALLOON_ULM_MCP73833_H


#include "Arduino.h"
#include "sensors/Ulm_Sensor.h"


enum Ulm_MCP73833_Status {
    SHUTDOWN,
    STANDBY,
    CHARGING,
    CHARGE_COMPLETE,
    TEMPERATURE_FAULT,
    SYSTEM_TEST_MODE,
    UNKNOWN
};


struct Ulm_MCP73833_Sample {
    Ulm_MCP73833_Status status;
    bool error;
};


class Ulm_MCP73833 final : public Ulm_Sensor<Ulm_MCP73833_Sample> {

private:
    uint8_t pinStat1;
    uint8_t pinStat2;
    uint8_t pinPG;
    Ulm_MCP73833_Status currentStatus;

public:
    Ulm_MCP73833(int8_t pinStat1, int8_t pinStat2, int8_t pinPg);
    bool begin() override;
    bool sample(Ulm_MCP73833_Sample &t) override;
    [[nodiscard]] const char * getDeviceName() const override;

private:
    void updateStatus();
    friend void ulm_mcp73833_interrupt_forwarder(void* ptr);

};


#endif //ULM_WEATHERBALLOON_ULM_MCP73833_H
