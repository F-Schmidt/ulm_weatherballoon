/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type LIS3MDL.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_LIS3MDL.h"


Ulm_LIS3MDL::Ulm_LIS3MDL(const uint8_t address, TwoWire &wire) : Ulm_I2C_Accessor(address, wire) {
    // Nothing more to do here.
}


bool Ulm_LIS3MDL::begin() {
    if(!begin_I2C(this->address, &this->i2c)) {
        return false;
    }
    this->setPerformanceMode(LIS3MDL_ULTRAHIGHMODE);
    this->setOperationMode(LIS3MDL_CONTINUOUSMODE); //TODO Change to single mode and test! better power consumption
    this->setDataRate(LIS3MDL_DATARATE_1_25_HZ);
    this->setRange(LIS3MDL_RANGE_16_GAUSS); // This is TBD...

    // Disable all interrupts.
    this->configInterrupt(false, false, false, false, false, false);
    return true;
}


bool Ulm_LIS3MDL::sample(Ulm_LIS3MDL_Sample &t) {
    sensors_event_t event;
    if(!this->getEvent(&event)) {
        return false;
    }

    t.magnetX = event.magnetic.x;
    t.magnetY = event.magnetic.y;
    t.magnetZ = event.magnetic.z;
    return true;
}


const char *Ulm_LIS3MDL::getDeviceName() const {
    return "LIS3MDL";
}
