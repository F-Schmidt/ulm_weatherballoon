//
// Created by Falko Alrik Schmidt on 11.01.24.
//

#ifndef ULM_WEATHERBALLOON_ULM_ADXL343_H
#define ULM_WEATHERBALLOON_ULM_ADXL343_H


#include "Adafruit_ADXL343.h"
#include "interfaces/Ulm_I2C_Accessor.h"
#include "sensors/Ulm_Sensor.h"

struct Ulm_ADXL343_Sample {
    float accelerationX;    // Unit: m/s^2
    float accelerationY;    // Unit: m/s^2
    float accelerationZ;    // Unit: m/s^2
};

class Ulm_ADXL343 : public Ulm_Sensor<struct Ulm_ADXL343_Sample>, Ulm_I2C_Accessor, Adafruit_ADXL343 {

public:
    /**
     * Constructor.
     * @param address the i2c address of the ADXL343 sensor.
     * @param wire the i2c interface.
     */
    explicit Ulm_ADXL343(uint8_t address = ADXL343_ADDRESS, TwoWire &wireBus = Wire);

    [[nodiscard]] const char *getDeviceName() const override;

    /**
     * Inits the sensor.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;

    bool sample(struct Ulm_ADXL343_Sample &t) override;

};


#endif //ULM_WEATHERBALLOON_ULM_ADXL343_H
