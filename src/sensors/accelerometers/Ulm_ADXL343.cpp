//
// Created by Falko Alrik Schmidt on 11.01.24.
//

#include "Ulm_ADXL343.h"

Ulm_ADXL343::Ulm_ADXL343(uint8_t address, TwoWire &wireBus) : Ulm_Sensor(), Ulm_I2C_Accessor(address, wireBus),
        Adafruit_ADXL343(-1, &wireBus) {
    // Nothing more to do here
}


bool Ulm_ADXL343::begin() {
    if(!Adafruit_ADXL343::begin(this->address)) {
        return false;
    }
    this->setRange(ADXL343_RANGE_2_G);
    this->setDataRate(ADXL343_DATARATE_1_56_HZ);
    return true;
}


bool Ulm_ADXL343::sample(struct Ulm_ADXL343_Sample &sample) {
    // Read out sensor.
    sensors_event_t event;
    if(!this->getEvent(&event)) {
        return false;
    }

    // Set values.
    sample.accelerationX = event.acceleration.x;
    sample.accelerationY = event.acceleration.y;
    sample.accelerationZ = event.acceleration.z;
    return true;
}

const char *Ulm_ADXL343::getDeviceName() const {
    return "ADXL343";
}
