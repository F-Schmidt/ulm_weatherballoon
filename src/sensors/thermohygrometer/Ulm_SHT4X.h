//
// Created by Falko Alrik Schmidt on 01.07.24.
//

#ifndef ULM_WEATHERBALLOON_ULM_SHT4X_H
#define ULM_WEATHERBALLOON_ULM_SHT4X_H


#include <Arduino.h>
#include <Wire.h>
#include "SensirionI2cSht4x.h"

#include "Ulm_Thermohygrometer.h"

typedef Ulm_Thermohygrometer_Sample Ulm_SHT4X_Sample;

/**
 * This class represents sensors of type SHT4X to measure temperature and relative humidity.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_SHT4X final : public Ulm_Thermohygrometer<Ulm_SHT4X_Sample>, SensirionI2cSht4x {

private:
    /**
     * The address, that this sensor uses.
     */
    uint8_t address;


    /**
     * The I2C bus, which this sensor is connected to.
     */
    TwoWire& wire;

public:
    /**
     * Default constructor.
     * @param address the i2c address, defaults to 0x44.
     * @param wire the i2c bus, defaults to Wire.
     */
    explicit Ulm_SHT4X(uint8_t address = 0x44, TwoWire& wire = Wire);


    /**
     * Default destructor.
     */
    ~Ulm_SHT4X() override = default;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /**
     * Initializes the sensor.
     * @return {@code true} on, and only on, success without ANY error, {@code false} otherwise.
     */
    bool begin() override;


    /**
     * Samples the sensor and reads out temperature and humidity.
     * @param t the sample to store data in.
     * @return {@code true} on success, {@code false} on error. Please note, that the sample
     * values are not updated on error.
     */
    bool sample(Ulm_SHT4X_Sample &t) override;
};


#endif //ULM_WEATHERBALLOON_ULM_SHT4X_H
