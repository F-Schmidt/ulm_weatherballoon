/*
* This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a generic
 * structure for temperature and humidity sensors.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_TEMPHUMSENSOR_H
#define ULM_TEMPHUMSENSOR_H

#include <cmath>

#include "sensors/thermometer/Ulm_Thermometer.h"


struct Ulm_Thermohygrometer_Sample : Ulm_Thermometer_Sample {
    float relativeHumidity{};
    float heatIndex{};
};


template<typename T = Ulm_Thermohygrometer_Sample>
class Ulm_Thermohygrometer : public Ulm_Thermometer<T> {

    /* Compile-time check for sample type matching. */
    static_assert(
        std::is_base_of_v<Ulm_Thermohygrometer_Sample, T>,
        "Temp/Hum sensor implementation must use a descendant of Ulm_TempHumSensor_Sample as generic type!"
    );


public:
    bool sample(T &sample) override {
        sample.heatIndex = calcHeatIndex(sample.temperature, sample.relativeHumidity);
        return Ulm_Thermometer<T>::sample(sample);
    }


private:
    static float calcHeatIndex(const float temperature, const float relativeHumidityPercent) {
        if (temperature <= 26.7F) {
            return NAN;
        }
        if (relativeHumidityPercent < 40.0F) {
            return NAN;
        }
        const float relativeHumidity = relativeHumidityPercent / 100.0F;
        return -8.784695F +
               1.61139411F * temperature +
               2.338549F * relativeHumidity +
               -0.14611605F * temperature * relativeHumidity +
               -1.2308094e-2F * temperature * temperature +
               -1.6424828e-2F * relativeHumidity * relativeHumidity +
               2.211732e-3F * temperature * temperature * relativeHumidity +
               7.2546e-4F * temperature * relativeHumidity * relativeHumidity +
               -3.582e-6F * temperature * temperature * relativeHumidity * relativeHumidity;
    }
};



#endif //ULM_TEMPHUMSENSOR_H
