//
// Created by Falko Alrik Schmidt on 01.07.24.
//

#include "Ulm_SHT4X.h"


Ulm_SHT4X::Ulm_SHT4X(const uint8_t address, TwoWire& wire) : address(address), wire(wire) {
    // Nothing more to do here.
}


bool Ulm_SHT4X::begin() {
    // Set up wire and sensor.
    this->wire.begin();
    SensirionI2cSht4x::begin(this->wire, this->address);

    // Perform a reset and check if it works.
    if (this->softReset() != 0) {
        return false;
    }

    // Read out serial number. This ensures, that CRC check is
    // successful and sensor and interface are working.
    uint32_t serialNumber;
    const int16_t error = SensirionI2cSht4x::serialNumber(serialNumber);
    return error == 0;
}


bool Ulm_SHT4X::sample(Ulm_SHT4X_Sample &t) {
    // These variables will be used to store measurements.
    float mTemp = NAN;
    float mHum = NAN;

    // Read values from sensor.
    if(this->measureHighPrecision(mTemp, mHum) != 0) {
        return false;
    }

    // Make sure, that the measured temperature is in range.
    if (mTemp < -40.0F || mTemp > 125.0F || mTemp == NAN) {
        return false;
    }

    // Crop humidity values to 0...100% according to datasheet section 4.6.
    if (mHum == NAN) {
        return false;
    }
    mHum = min(max(0, mHum), 100.0F);

    // Transfer read values over to given struct and return.
    t.temperature = mTemp;
    t.relativeHumidity = mHum;
    return Ulm_Thermohygrometer::sample(t);
}


const char *Ulm_SHT4X::getDeviceName() const {
    return "SHT4x";
}
