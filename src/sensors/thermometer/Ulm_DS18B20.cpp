/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type DS18B20.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_DS18B20.h"

#include "Ulm_Thermometer.h"

Ulm_DS18B20::Ulm_DS18B20(const uint8_t pin) {
    this->oneWire = OneWire(pin);       // This is to not use heap.
    this->setOneWire(&this->oneWire); // This is to not use heap.
    this->setWaitForConversion(false);  // Async mode (non-blocking).
}


const char *Ulm_DS18B20::getDeviceName() const {
    return "DS18B20";
}


bool Ulm_DS18B20::begin() {
    // Start interface.
    DallasTemperature::begin();

    // Check if device count is correct.
    // If not, we will directly return false and
    // there is an interface or sensor error.
    if(this->getDeviceCount() != 1) {
        return false;
    }

    // Set resolution of sensor to 12 bits, which gives
    // the highest resolution at a conversion time of
    // max 750 ms.
    this->setResolution(12);
    return this->getResolution() == 12;
}


bool Ulm_DS18B20::sample(Ulm_DS18B20_Sample &sample) {
    // Request sample from sensor.
    this->requestTemperaturesByIndex(0);

    // Read temperature and validate. If the temperature
    // is invalid (due to sensor error), then this
    // method will return false and not update the
    // given float. The check against -55...125C also
    // checks for sensor faults (open, short GND, short VCC).
    const float temperature = this->getTempCByIndex(0);
    if(temperature < -55.0F || temperature > 125.0F) {
        return false;
    }

    // In this case, the read temperature is valid. The
    // given float will be updated and true will be
    // returned.
    sample.temperature = temperature;
    return Ulm_Thermometer::sample(sample);
}
