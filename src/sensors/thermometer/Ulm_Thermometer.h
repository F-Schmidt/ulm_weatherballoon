/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides a generic
 * structure for temperature sensors.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_TEMPERATURESENSOR_H
#define ULM_TEMPERATURESENSOR_H

#include <cmath>
#include <type_traits>
#include <sensors/Ulm_Sensor.h>

/**
 * Generic temperature sensor sample.
 */
struct Ulm_Thermometer_Sample {
    float temperature{};
    float speedOfSound{};
    float saturationVaporPressure{};
};


/**
 * This class represents a sensor, that can measure the temperature, but no further values.
 *
 * @tparam T the sample type, which must be a subtype of Ulm_TemperatureSensor_Sample.
 * @author Falko Schmidt
 * @since 0.2.7
 */
template<typename T = Ulm_Thermometer_Sample>
class Ulm_Thermometer : public Ulm_Sensor<T> {

    /* Compile-time check for sample type matching. */
    static_assert(
        std::is_base_of_v<Ulm_Thermometer_Sample, T>,
        "Temperature sensor implementation must use a descendant of Ulm_TemperatureSensor_Sample as generic type!"
    );


public:
    /**
     * This implementation calculates some values within the structure according to the read-out values from
     * the sensor. Each sensor must inherit this method, read out the sensor and call this method at the end
     * of the sampling routine.
     * @param sample the sample to store data in.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool sample(T &sample) override {
        sample.speedOfSound            = calcSpeedOfSound(sample.temperature);
        sample.saturationVaporPressure = calcSaturationVaporPressure(sample.temperature);
        return true;
    }


private:
    /**
     * Calculates the speed of sound in regard to the current temperature.
     * @param temperature the current temperature.
     * @return the speed of sound dependent on the current temperature.
     *
     * Source for this formula: https://de.wikipedia.org/wiki/Schallgeschwindigkeit
     */
    static float calcSpeedOfSound(const float temperature) {
        static constexpr float HEAT_CAPACITY_RATION_AIR = 1.40F;
        static constexpr float GAS_CONSTANT_AIR = 287.05F;
        return sqrt(HEAT_CAPACITY_RATION_AIR * GAS_CONSTANT_AIR * (temperature + 273.15));
    }


    /**
     * Calculates the vapor pressure in hPa in regard to the current temperature using
     * the magnus-formula.
     * @param temperature the current temperature.
     * @return the vapor pressure dependent on the current temperature.
     *
     * Source for this formula: https://de.wikipedia.org/wiki/Sättigungsdampfdruck
     */
    static float calcSaturationVaporPressure(const float temperature) {
        return 6.112F * exp((17.62F * temperature) / (243.12F + temperature));
    }
};


#endif //ULM_TEMPERATURESENSOR_H
