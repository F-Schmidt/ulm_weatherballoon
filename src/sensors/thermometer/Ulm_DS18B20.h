/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type DS18B20.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H
#define UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H

#include "DallasTemperature.h"
#include "Ulm_Thermometer.h"

typedef Ulm_Thermometer_Sample Ulm_DS18B20_Sample;

/**
 * This class represents temperature sensors of type DS18B20.
 *
 * @author Falko Schmidt
 * @since 0.1.9
 */
class Ulm_DS18B20 final : DallasTemperature, public Ulm_Thermometer<Ulm_DS18B20_Sample> {

private:
    /**
     * The OneWire interface handle, that is used to communicate
     * with the temperature sensor of type 'DS18B20'.
     */
    OneWire oneWire;


public:
    /**
     * Default constructor.
     * @param pin the pin, which the sensor is connected to.
     */
    explicit Ulm_DS18B20(uint8_t pin);


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the temperature sensor name.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /**
     * Initializes the temperature sensor.
     * @return {@code true} if everything is successful and exactly
     * one temperature sensor is found, {@code false} otherwise.
     */
    bool begin() override;

    /**
     * Reads out the temperature from sensor.
     * @param sample the sample to store read values in.
     * @return {@code true} on success, {@code false} if there is an
     * error with the sample or sensor.
     */
    bool sample(Ulm_DS18B20_Sample &sample) override;
};


#endif //UULM_WEATHERBALLOON_ARDUINO_FRAMEWORK_TEST_ULM_TEMPERATURESENSOR_DS18B20_H
