/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type MCP9700.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_MCP9700_H
#define ULM_WEATHERBALLOON_ULM_MCP9700_H


#include "Arduino.h"
#include "Ulm_Thermometer.h"
#include "Ulm_Weatherballoon.h"

typedef Ulm_Thermometer_Sample Ulm_MCP9700_Sample;

/**
 * This class implements controls for temperature sensors of type MCP9700.
 *
 * @since 0.2.0
 * @author Falko Schmidt
 */
class Ulm_MCP9700 final : public Ulm_Thermometer<Ulm_MCP9700_Sample> {

private:
    /**
     * The analog pin, which the sensor output is connected to.
     */
    const uint8_t adcPin;


    /**
     * The reference voltage of the built-in ADC.
     */
    const float adcVoltage;


    /**
     * The resolution of the built-in ADC in bits.
     */
    const uint8_t adcResolutionBits;


public:
    /**
     * Default constructor. The only required parameter is adcPin.
     * @param adcPin the pin, which the sensor is connected to.
     * @param adcVoltage the voltage level of ADC. Typically, this is the MCU supply voltage.
     * @param adcResolutionBits the resolution of ADC in bits.
     */
    explicit Ulm_MCP9700(uint8_t adcPin, float adcVoltage = 3.3f, uint8_t adcResolutionBits = 10);


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the temperature sensor name.
     */
    [[nodiscard]] const char *getDeviceName() const override;


    /**
     * Sets up sensor. This method must be called before read out.
     * In this case, the analog pin will be initialised.
     * @return always {@code true}.
     */
    bool begin() override;


    /**
     * Reads out the current temperature value. Please note, that the given float will only
     * be updated, if the read-out temperature value is within sensor specs. Otherwise, this
     * method will return false and NOT write the value to given float.
     * @param sample the float, which the current temperature is written to.
     * @return {@code} true on success, {@code false} if the temperature value is not
     * within sensor specs and therefore invalid.
     */
    bool sample(Ulm_MCP9700_Sample &sample) override;
};


#endif //ULM_WEATHERBALLOON_ULM_MCP9700_H
