/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type MCP9700.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_MCP9700.h"


Ulm_MCP9700::Ulm_MCP9700(const uint8_t adcPin, const float adcVoltage, const uint8_t adcResolutionBits)
        : adcPin(adcPin), adcVoltage(adcVoltage), adcResolutionBits(adcResolutionBits) {
    // Nothing to do here.
}


const char *Ulm_MCP9700::getDeviceName() const {
    return "MCP9700";
}


bool Ulm_MCP9700::begin() {
    if(!PinUtil::isAnalogPin(this->adcPin)) {
        return false;
    }
    pinMode(this->adcPin, INPUT);
    return true;
}


bool Ulm_MCP9700::sample(Ulm_MCP9700_Sample &sample) {
    // Read out ADC to get voltage from sensor.
    const auto readADCValue = static_cast<float>(analogRead(this->adcPin));
    const auto maxADCValue  = static_cast<float>(1 << this->adcResolutionBits);

    const float voltage = readADCValue / maxADCValue * this->adcVoltage;
    const float temperature = (voltage - 0.5F) / 0.01F;

    // Check, if the temperature is within sensor spec. If it is not, then the
    // sampled values are invalid and this method will return directly.
    if(temperature < -40.0F || temperature > 125.0F) {
        return false;
    }

    // Sensor values are only updated, if the read value is within
    // sensor specification (-40,...,125 Celsius)
    sample.temperature = temperature;
    return Ulm_Thermometer::sample(sample);
}
