/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type SCD4X.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_SCD4X_H
#define ULM_WEATHERBALLOON_ULM_SCD4X_H


#include <sensors/Ulm_Sensor.h>

#include "SensirionI2cScd4x.h"
#include "interfaces/Ulm_I2C_Accessor.h"


/**
 * Default sample type for SCD4x sensor.
 */
struct Ulm_SCD4X_Sample {
    uint16_t co2{};      // <- the unit is ppm (parts per million)
    float co2Percent{};  // <- the unit is %
    float temperature{};
    float relativeHumidity{};
};


/**
 * This class implements the controller for sensors of type SCD4X.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_SCD4X final : SensirionI2cScd4x, public Ulm_Sensor<Ulm_SCD4X_Sample>, public Ulm_I2C_Accessor {


public:
    /**
      * Default constructor.
      */
    explicit Ulm_SCD4X(uint8_t address = 0x62, TwoWire& wire = Wire);

    
    /**
     * Initializes the sensor.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;


    /**
     * Reads out CO2 sensor.
     * @param sample the sensor sample to store data into.
     * @return {@code true} on success, {@code false} on error.
     */
    bool sample(Ulm_SCD4X_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char * getDeviceName() const override;
};

#endif //ULM_WEATHERBALLOON_ULM_SCD4X_H
