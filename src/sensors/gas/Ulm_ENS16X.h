/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type ENS160 and ENS161.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_WEATHERBALLOON_ULM_ENS16X_H
#define ULM_WEATHERBALLOON_ULM_ENS16X_H


#include <sensors/Ulm_Sensor.h>

#include "ScioSense_ENS160.h"
#include "interfaces/Ulm_I2C_Accessor.h"


/**
 * Data sample for ENS160/ENS161 sensors.
 */
struct Ulm_ENS16X_Sample {
    uint16_t eCo2;      // <- unit is ppm
    uint16_t tvoc;      // <- unit is ppb (Parts per billion)
    float tvocPercent;  // <- unit is %
    uint8_t aqi;        // Air quality index from 0 to 5
    uint16_t aqi500;    // Air quality index from 0 to 500
};


/**
 * This class allows to control sensors of type ENS160 and ENS161.
 *
 * @author Falko Schmidt
 * @since 0.2.7
 */
class Ulm_ENS16X final : ScioSense_ENS160, public Ulm_Sensor<Ulm_ENS16X_Sample>, public Ulm_I2C_Accessor {


public:
    /**
     * Default Constructor.
     * @param address the i2c address of the ENS16X sensor.
     * @param wire the i2c interface.
     */
    explicit Ulm_ENS16X(uint8_t address = 0x52, TwoWire &wire = Wire);


    /**
     * Inits the sensor.
     * @return true on success, false otherwise.
     */
    bool begin() override;


    /**
     * Reads out a sensor of type ENS16X.
     * @param sample the sample to store the read data into.
     * @return true on success, false otherwise.
     */
    bool sample(Ulm_ENS16X_Sample &sample) override;


    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;

};

#endif //ULM_WEATHERBALLOON_ULM_ENS16X_H
