/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation reads out sensors of
 * type SCD4X.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_SCD4X.h"


Ulm_SCD4X::Ulm_SCD4X(const uint8_t address, TwoWire& wire) : Ulm_Sensor(), Ulm_I2C_Accessor(address, wire) {
    // Nothing more to do here.
}


bool Ulm_SCD4X::begin() {
    if (!this->startInterface()) {
        return false;
    }

    // Start sensor and give it some time.
    SensirionI2cScd4x::begin(this->i2c, this->address);
    delay(30);

    if (this->wakeUp() != 0) {
        return false;
    }

    if (this->stopPeriodicMeasurement() != 0) {
        return false;
    }

    if (this->reinit() != 0) {
        return false;
    }

    if (uint64_t serialNumber; this->getSerialNumber(serialNumber) != 0) {
        return false;
    }

    return this->startPeriodicMeasurement();
}


bool Ulm_SCD4X::sample(Ulm_SCD4X_Sample &sample) {

    bool isDataReady = false;
    if (this->getDataReadyStatus(isDataReady) != 0) {
        return false;
    }

    // In this case return true, because this is not an error.
    if (!isDataReady) {
        return true;
    }

    uint16_t co2; float temp; float hum;
    if (this->readMeasurement(co2, temp, hum) != 0) {
        return false;
    }

    sample.co2              = co2;
    sample.co2Percent       = static_cast<float>(co2) / 10000.0F;
    sample.temperature      = temp;
    sample.relativeHumidity = hum;
    return true;
}


const char * Ulm_SCD4X::getDeviceName() const {
    return "SCD4X";
}
