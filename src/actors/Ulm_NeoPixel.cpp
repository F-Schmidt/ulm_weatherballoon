/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation can control a single
 * 'NeoPixel' RGB LED.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_NeoPixel.h"

#include <string>


Ulm_NeoPixel::Ulm_NeoPixel(const int16_t pin) : Adafruit_NeoPixel(1, pin, NEO_GRB + NEO_KHZ800) {
    // Nothing to do here.
}


bool Ulm_NeoPixel::begin() {
    Adafruit_NeoPixel::begin();
    this->setBrightness(100);
    this->off();
    return true;
}


void Ulm_NeoPixel::off() {
    this->clear();
    this->show();
}


void Ulm_NeoPixel::showColor(const uint32_t color) {
    this->setPixelColor(0, color);
    this->show();
}


void Ulm_NeoPixel::showColor(const Ulm_NeoPixel_Color color) {
    this->showColor(static_cast<uint32_t>(color));
}


const char *Ulm_NeoPixel::getDeviceName() const {
    static std::string deviceName = "Single NeoPixel RGB LED (Pin #" + std::to_string(this->pin) + ')';
    return deviceName.c_str();
}
