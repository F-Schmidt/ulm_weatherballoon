//
// Created by Falko Alrik Schmidt on 22.02.25.
//

#ifndef ULM_THERMALPRINTER_H
#define ULM_THERMALPRINTER_H


#include <Adafruit_Thermal.h>
#include <Ulm_Device.h>
#include <util/Ulm_Testable.h>


class Ulm_ThermalPrinter final : Adafruit_Thermal, public Ulm_Device, Ulm_Testable {
public:
    explicit Ulm_ThermalPrinter(HardwareSerial &serial = Serial1); //TODO automatisch HW/SW verwenden?
    bool begin() override;
    [[nodiscard]] const char * getDeviceName() const override;

private:
    bool runSelfTest() override;
};


#endif //ULM_THERMALPRINTER_H
