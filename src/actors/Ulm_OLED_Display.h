/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation can control the very
 * common I2C displays with a resolution of 128 x 64 pixels based on
 * the SSD1306 or similar controlling circuits.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H

#include "Ulm_Device.h"
#include "Adafruit_SSD1306.h"

/**
 * This class represents an OLED with dimensions of 128 x 64 pixels
 * which is connected via i2c at address 0x3C.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_OLED_Display : public Adafruit_SSD1306, public Ulm_Device {

private:
    uint8_t address;

public:
    /**
     * Default constructor.
     */
    explicit Ulm_OLED_Display(uint8_t address = 0x3C, TwoWire& wire = Wire);

    /**
     * Initializes the display.
     * After calling this method, the display can be written to.
     * @return {@code true} on success, {@code false} otherwise.
     */
    bool begin() override;

    /**
     * Writes the given text horizontally centered at the given y coordinate.
     * @param y the vertical position to write the text at.
     * @param s the text to write.
     */
    void writeCentered(uint8_t y, const String& s);

    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char * getDeviceName() const override;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_OLED_DISPLAY_H
