//
// Created by Falko Alrik Schmidt on 23.02.25.
//

#include "Ulm_TFT.h"

Ulm_TFT::Ulm_TFT(SPIClass &spiClass, int8_t dc, int8_t cs, int8_t rst) : Adafruit_ILI9341(&spiClass, dc, cs, rst) {
    // Nothing to do here.
}


bool Ulm_TFT::begin() {
    // Start display with default SPI frequency.
    Adafruit_ILI9341::begin(0);

    // No detectable errors.
    return true;
}


const char * Ulm_TFT::getDeviceName() const {
    return "TFT-display (320x240, ILI9341)";
}
