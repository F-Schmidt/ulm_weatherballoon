/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. This class allows the easy usage of
 * debounced buttons.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_Button.h"


void ulmButtonHandleInterruptFalling(void *ptr) { static_cast<Ulm_Button *>(ptr)->handleInterruptFalling(); }


void ulmButtonHandleInterruptRising(void *ptr) { static_cast<Ulm_Button *>(ptr)->handleInterruptRising(); }


Ulm_Button::Ulm_Button(const uint8_t pin, const Mode mode, const uint16_t debounceMillis) :
    pin(pin), mode(mode), debounceMillis(debounceMillis) {
    this->callbackFalling = nullptr;
    this->callbackRising = nullptr;
    this->lastFallingMillis = 0;
    this->lastRisingMillis = 0;
}


bool Ulm_Button::begin() {
    // Setup pin with according pull resistor.
    pinMode(this->pin, this->mode == ON_PRESS_LOW ? INPUT_PULLUP : INPUT_PULLDOWN);

    // Attach callback methods from this class.
    attachInterruptParam(digitalPinToInterrupt(this->pin), ulmButtonHandleInterruptFalling, FALLING, this);
    attachInterruptParam(digitalPinToInterrupt(this->pin), ulmButtonHandleInterruptRising, RISING, this);
    return true;
}


void Ulm_Button::handleInterruptFalling() {
    const unsigned long now = millis();
    if (now - this->lastFallingMillis <= this->debounceMillis) {
        // Ignore all bounces that are within the debounce millisecond time frame.
        return;
    }
    this->lastFallingMillis = now;
    if (this->callbackFalling == nullptr) {
        return;
    }
    this->callbackFalling();
}


void Ulm_Button::handleInterruptRising() {
    const unsigned long now = millis();
    if (now - this->lastRisingMillis <= this->debounceMillis) {
        // Ignore all bounces that are within the debounce millisecond time frame.
        return;
    }
    this->lastRisingMillis = now;
    if (this->callbackRising == nullptr) {
        return;
    }
    this->callbackRising();
}


const char *Ulm_Button::getDeviceName() const { return "Debounced button"; }


bool Ulm_Button::isPressed() const { return digitalRead(this->pin) == (this->mode == ON_PRESS_LOW ? LOW : HIGH); }


bool Ulm_Button::isReleased() const { return !this->isPressed(); }


void Ulm_Button::onFalling(void (*f)()) { this->callbackFalling = f; }


void Ulm_Button::onRising(void (*f)()) { this->callbackRising = f; }
