/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides an object-
 * oriented approach to control the onboard-LED.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H

#include "Ulm_LED.h"

/**
 * This class represents the built-in LED of an Arduino.
 *
 * @since 1.0
 * @author Falko Schmidt
 */
class Ulm_LED_BuiltIn final : public Ulm_LED {

public:
    /**
     * Default constructor. This will construct an object of type
     * {@code Ulm_LED} at pin {@code LED_BUILTIN}.
     */
    explicit Ulm_LED_BuiltIn();

    /**
     * Toggles the LED. If the LED was on before, then calling this method will
     * turn off the LED, otherwise the LED will be turned on.
     */
    [[nodiscard]] const char *getDeviceName() const override;
};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_LED_BUILTIN_H
