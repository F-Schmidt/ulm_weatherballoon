/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation provides an object-
 * oriented approach to control an LED.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <Arduino.h>
#include "Ulm_LED.h"

#include <string>

Ulm_LED::Ulm_LED(const uint8_t pin, const Mode mode) : pin(pin), mode(mode) {
    // Nothing more to do here.
}


bool Ulm_LED::begin() {
    pinMode(this->pin, OUTPUT);
    this->off();
    return true;
}


void Ulm_LED::on(const uint8_t brightness) const {
    switch (brightness) {
        case 0:
            this->off();
            break;
        case 255:
            digitalWrite(this->pin, this->mode == ACTIVE_HIGH ? HIGH : LOW);
            break;
        default:
            analogWrite(this->pin, brightness);
            break;
    }
}


void Ulm_LED::off() const {
    digitalWrite(this->pin, this->mode == ACTIVE_HIGH ? LOW : HIGH);
}


void Ulm_LED::toggle() const {
    // Use digitalRead to fetch the current pin value, then invert it and write back.
    digitalWrite(this->pin, digitalRead(this->pin) == HIGH ? LOW : HIGH);
}


const char *Ulm_LED::getDeviceName() const {
    static std::string deviceName = "LED (Pin #" + std::to_string(this->pin) + ')';
    return deviceName.c_str();
}
