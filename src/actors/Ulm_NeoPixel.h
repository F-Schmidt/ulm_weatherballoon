/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation can control a single
 * 'NeoPixel' RGB LED.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H
#define UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H


#include "Adafruit_NeoPixel.h"
#include "Ulm_Device.h"


/**
 * Calculates RGB color codes.
 * @param r the 'red' component of the color from 0 to 255.
 * @param g the 'green' component of the color from 0 to 255.
 * @param b the 'blue' component of the color from 0 to 255.
 * @return the rgb color code, that can directly be sent to neopixel LEDs.
 */
constexpr uint32_t dUlm_NeoPixel_Color(const uint8_t r, const uint8_t g, const uint8_t b) {
    return (static_cast<uint32_t>(r) << 16) |
           (static_cast<uint32_t>(g) << 8)  |
            static_cast<uint32_t>(b);
}


/**
 * Some basic color definitions for use with NeoPixel LEDs.
 */
enum class Ulm_NeoPixel_Color : uint32_t {
    RED     = dUlm_NeoPixel_Color(255, 0, 0),
    GREEN   = dUlm_NeoPixel_Color(0, 255, 0),
    BLUE    = dUlm_NeoPixel_Color(0, 0, 255),
    WHITE   = dUlm_NeoPixel_Color(255, 255, 255),
    BLACK   = dUlm_NeoPixel_Color(0, 0, 0),
    YELLOW  = dUlm_NeoPixel_Color(255, 255, 0),
    CYAN    = dUlm_NeoPixel_Color(0, 255, 255),
    MAGENTA = dUlm_NeoPixel_Color(255, 0, 255),
    ORANGE  = dUlm_NeoPixel_Color(255, 165, 0),
    PURPLE  = dUlm_NeoPixel_Color(128, 0, 128),
    PINK    = dUlm_NeoPixel_Color(255, 192, 203),
    GRAY    = dUlm_NeoPixel_Color(128, 128, 128),
    BROWN   = dUlm_NeoPixel_Color(139, 69, 19)
};


/**
 * This class represents an RGB LED of type WS2812, WS2813 or similar.
 *
 * @author Falko Schmidt
 * @since 1.0
 */
class Ulm_NeoPixel final : public Adafruit_NeoPixel, public Ulm_Device {

public:
    /**
     * Default constructor.
     * @param pin the pin, which the RGB LED is connected to.
     */
    explicit Ulm_NeoPixel(int16_t pin);

    /**
     * Inits the RGB LED and turns it off.
     * @return always {@code true}.
     */
    bool begin() override;

    /**
     * Turns off the NeoPixel LED.
     */
    void off();

    /**
     * Returns a descriptive string, that can be used for debugging.
     * @return a string containing the device name / article number.
     */
    [[nodiscard]] const char *getDeviceName() const override;

    /**
     * Turns the NeoPixel LED on in a specific color.
     * @param color the color to be displayed.
     */
    void showColor(uint32_t color);

    /**
     * Turns the NeoPixel LED on in a specific color.
     * @param color the color to be displayed.
     */
    void showColor(Ulm_NeoPixel_Color color);

};


#endif //UULM_WEATHERBALOON_ARDUINO_FRAMEWORK_TEST_ULM_RGB_LED_H
