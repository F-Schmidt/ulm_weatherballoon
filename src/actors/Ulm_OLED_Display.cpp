/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. The implementation can control the very
 * common I2C displays with a resolution of 128 x 64 pixels based on
 * the SSD1306 or similar controlling circuits.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2024, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Ulm_OLED_Display.h"

#include <string>

Ulm_OLED_Display::Ulm_OLED_Display(const uint8_t address, TwoWire& wire)
        : Adafruit_SSD1306(128, 64, &wire, -1), address(address) {
    // Nothing more to do here.
}


bool Ulm_OLED_Display::begin() {
    if(!Adafruit_SSD1306::begin(SSD1306_SWITCHCAPVCC, this->address)) {
        return false;
    }
    this->setTextSize(1);
    this->setTextColor(SSD1306_WHITE, SSD1306_BLACK);
    this->clearDisplay();
    this->display();
    return true;
}


void Ulm_OLED_Display::writeCentered(const uint8_t y, const String &s) {

    // Get bounds of given text
    int16_t xb, yb; uint16_t w, h;
    getTextBounds(s.c_str(), this->cursor_x, this->cursor_y, &xb, &yb, &w, &h);

    // Set cursor to correct position and write text
    this->setCursor(static_cast<int16_t>((this->width() - w)/2), y);
    this->write(s.c_str());
}


const char * Ulm_OLED_Display::getDeviceName() const {
    static std::string deviceName = "SSD1306 OLED 128x64px (" + std::to_string(this->address) + '@' +
        (this->wire == &Wire ? "I2C0" : "I2C1" + ')');
    return deviceName.c_str();
}
