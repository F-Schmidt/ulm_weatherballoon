//
// Created by Falko Alrik Schmidt on 23.02.25.
//

#ifndef ULM_TFT_H
#define ULM_TFT_H
#include <Adafruit_ILI9341.h>
#include <Ulm_Device.h>


class Ulm_TFT final : public Ulm_Device, public Adafruit_ILI9341 {
public:
    explicit Ulm_TFT(SPIClass &spiClass = SPI1, int8_t dc = 2, int8_t cs = 13, int8_t rst = 3);
    bool begin() override;
    [[nodiscard]] const char * getDeviceName() const override;
};



#endif //ULM_TFT_H
