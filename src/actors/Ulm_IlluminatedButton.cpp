//
// Created by Falko Alrik Schmidt on 23.02.25.
//

#include "Ulm_IlluminatedButton.h"

Ulm_IlluminatedButton::Ulm_IlluminatedButton(const uint8_t btnPin, const uint8_t ledPin, const Ulm_Button::Mode btnMode,
    const uint16_t debounceMillis, const Ulm_LED::Mode ledMode)
        : Ulm_LED(ledPin, ledMode), Ulm_Button(btnPin, btnMode, debounceMillis) {
    // Nothing more to do here.
}


bool Ulm_IlluminatedButton::begin() {
    return Ulm_LED::begin() && Ulm_Button::begin();
}


const char * Ulm_IlluminatedButton::getDeviceName() const {
    return "Debounced button with illumination";
}
