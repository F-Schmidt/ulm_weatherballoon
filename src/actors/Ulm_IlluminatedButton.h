//
// Created by Falko Alrik Schmidt on 23.02.25.
//

#ifndef ULM_ILLUMINATEDBUTTON_H
#define ULM_ILLUMINATEDBUTTON_H

#include "Ulm_Button.h"
#include "Ulm_LED.h"


// Virtual child of Ulm_LED to prevent diamond problem due to same super class of Ulm_Button and Ulm_LED.
class Ulm_IlluminatedButton final : public Ulm_Button, public virtual Ulm_LED {

public:
    explicit Ulm_IlluminatedButton(uint8_t btnPin, uint8_t ledPin,
                          Ulm_Button::Mode btnMode = ON_PRESS_LOW, uint16_t debounceMillis = 1000,
                          Ulm_LED::Mode ledMode = ACTIVE_HIGH);

    bool begin() override;
    [[nodiscard]] const char * getDeviceName() const override;
};


#endif //ULM_ILLUMINATEDBUTTON_H
