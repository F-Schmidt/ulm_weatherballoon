/*
 * This software is specifically designed for the high altitude balloon
 * project at Ulm University. This class allows the easy usage of
 * debounced buttons.
 *
 * Software License Agreement (BSD License)
 *
 * Copyright (c) 2025, Falko Schmidt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ULM_BUTTON_H
#define ULM_BUTTON_H

#include <Ulm_Device.h>

#include "Arduino.h"

class Ulm_Button : public Ulm_Device {

public:
    enum Mode { ON_PRESS_LOW, ON_PRESS_HIGH };

private:
    uint8_t pin;
    Mode mode;
    uint16_t debounceMillis;
    void (*callbackFalling)();
    void (*callbackRising)();
    unsigned long lastFallingMillis;
    unsigned long lastRisingMillis;

public:
    explicit Ulm_Button(uint8_t pin, Mode mode = ON_PRESS_LOW, uint16_t debounceMillis = 1000);
    bool begin() override;
    [[nodiscard]] const char *getDeviceName() const override;

    [[nodiscard]] bool isPressed() const;
    [[nodiscard]] bool isReleased() const;

    void onFalling(void (*f)());
    void onRising(void (*f)());

private:
    void handleInterruptFalling();
    void handleInterruptRising();

    friend void ulmButtonHandleInterruptFalling(void *ptr);
    friend void ulmButtonHandleInterruptRising(void *ptr);
};


#endif // ULM_BUTTON_H
