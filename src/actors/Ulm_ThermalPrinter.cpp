//
// Created by Falko Alrik Schmidt on 22.02.25.
//

#include "Ulm_ThermalPrinter.h"


Ulm_ThermalPrinter::Ulm_ThermalPrinter(HardwareSerial &serial) : Adafruit_Thermal(&serial) {
    // Nothing to do here.
}


bool Ulm_ThermalPrinter::begin() {
    // Start with the firmware of printer.
    Adafruit_Thermal::begin(268);   // TODO this must be adapted!!

    // Set default text formatting.
    this->setDefault();

    // Configure the heating of printer.
    this->setHeatConfig(11, 120, 40); //TODO this must be adapted!!

    // Do not accept any printing commands.
    // This will prevent weird symbols from
    // being printed due to eventual signals
    // on RX/TX lines.
    this->offline();

    // No detectable errors.
    return true;
}


const char * Ulm_ThermalPrinter::getDeviceName() const {
    return "Thermal Printer";
}


bool Ulm_ThermalPrinter::runSelfTest() {
    // Prints test page containing all information
    // about the printer settings.
    this->testPage();

    // No detectable errors.
    return true;
}
