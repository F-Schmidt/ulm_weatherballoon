//
// Created by Falko Alrik Schmidt on 22.08.24.
//

#ifndef ULM_WEATHERBALLOON_ULM_TESTABLE_H
#define ULM_WEATHERBALLOON_ULM_TESTABLE_H

/**
 * This class can be inherited by any class, that can execute a self test
 * in any form.
 *
 * @author Falko Schmidt
 * @since 0.2.6
 */
class Ulm_Testable {

    /**
     * Calling this method runs a test routine.
     * @return {@code true}, if there is either no error or no error can be detected,
     * {@code false} if there was an error.
     */
    virtual bool runSelfTest() = 0;

};

#endif //ULM_WEATHERBALLOON_ULM_TESTABLE_H
