//
// Created by Falko Alrik Schmidt on 17.08.24.
//

#ifndef ULM_WEATHERBALLOON_PINUTIL_H
#define ULM_WEATHERBALLOON_PINUTIL_H

#include "Arduino.h"

class PinUtil {

  	// TODO Check this and maybe not make it a class.
        //  only support RP2040 and RP2035
public:

    /**
     * Checks, if a given pin is connected to an ADC.
     * @param pin the pin to check
     * @return {@code true} if pin is analog, {@code false} if the
     * pin is either not analog or the architecture is not supported.
     */
    static bool isAnalogPin(const uint8_t pin) {
#if defined(PICO_RP2040)
        // Pico and Pico W
        switch (pin) {
            case A0:
            case A1:
            case A2:
            case A3: return true;
            default: return false;
        }
#else
        return false;
#endif
    }
};

#endif //ULM_WEATHERBALLOON_PINUTIL_H
